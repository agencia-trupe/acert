<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'     => 'trupe',
            'email'    => 'contato@trupe.net',
            'password' => bcrypt('senhatrupe'),
        ]);

        DB::table('admin_usuarios')->insert([
            'nome'       => 'Trupe Admin',
            'email'      => 'trupe@trupe.net',
            'password'   => bcrypt('senhatrupe'),
            'created_at' => Carbon::now()
        ]);
    }
}
