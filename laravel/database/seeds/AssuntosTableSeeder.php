<?php

use Illuminate\Database\Seeder;

class AssuntosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assuntos')->insert([
            [
                'ordem'  => 0,
                'titulo' => 'Solicitação de Orçamento',
            ],
            [
                'ordem'  => 1,
                'titulo' => 'Reclamações',
            ],
            [
                'ordem'  => 2,
                'titulo' => 'Sugestões',
            ],
            [
                'ordem'  => 3,
                'titulo' => 'Informações',
            ],
        ]);
    }
}
