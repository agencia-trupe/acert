<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'contato@trupe.net',
            'telefone' => '+55 11 94308 0882',
            'endereco' => 'Al. Lorena, 131 - cj. 133 - sala 2 &middot; Jardim Paulista<br />01424-000 &middot; São Paulo - SP &middot; Brasil',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.870221181024!2d-46.658786184474245!3d-23.573104084676945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59c2303d233d%3A0xdec42d761c317d50!2sAlameda%20Lorena%2C%20131%20-%20Jardim%20Paulista%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2001424-000!5e0!3m2!1spt-BR!2sbr!4v1592694788695!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
            'linkedin' => 'https://www.linkedin.com',
            'instagram' => 'https://www.instagram.com',
            'facebook' => 'https://www.facebook.com',
            'twitter' => 'https://www.twitter.com',
        ]);
    }
}
