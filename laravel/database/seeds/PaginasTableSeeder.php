<?php

use Illuminate\Database\Seeder;

class PaginasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginas')->insert([
            [
                'ordem'  => 0,
                'titulo' => 'Inspeções',
                'slug'   => 'inspecoes',
            ],
            [
                'ordem'  => 1,
                'titulo' => 'Certificação de Produtos',
                'slug'   => 'certificacao-de-produtos',
            ],
            [
                'ordem'  => 2,
                'titulo' => 'Certificação de Sistemas de Gestão',
                'slug'   => 'certificacao-de-sistemas-de-gestao',
            ],
            [
                'ordem'  => 3,
                'titulo' => 'Avaliações Técnicas',
                'slug'   => 'avaliacoes-tecnicas',
            ],
            [
                'ordem'  => 4,
                'titulo' => 'Treinamento',
                'slug'   => 'treinamento',
            ],
        ]);
    }
}
