<?php

use Illuminate\Database\Seeder;

class SistemaSeeder extends Seeder
{
    public function run()
    {
        DB::table('sistema')->insert([
            'auditores' => 'Acesso exclusivo para os auditores cadastrados',
            'imagem_auditores' => '',
            'inspecoes' => 'INMETRO &middot; Poder Público<br />Clientes &middot; Concessionárias',
            'imagem_inspecoes' => '',
            'administrador' => 'Acesso exclusivo de administração da Acert Inspeções',
            'imagem_administrador' => '',
        ]);
    }
}
