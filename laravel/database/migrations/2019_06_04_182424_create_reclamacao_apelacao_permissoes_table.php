<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReclamacaoApelacaoPermissoesTable extends Migration
{
    public function up()
    {
        Schema::create('sistema_reclamacao_apelacao_permissoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('reclamacao_id')->unsigned();
            $table->foreign('reclamacao_id')->references('id')->on('sistema_reclamacao_apelacao')->onDelete('cascade');

            $table->integer('usuario_id');
            $table->string('usuario_type');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sistema_reclamacao_apelacao_permissoes');
    }
}
