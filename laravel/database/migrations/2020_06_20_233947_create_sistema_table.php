<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemaTable extends Migration
{
    public function up()
    {
        Schema::create('sistema', function (Blueprint $table) {
            $table->increments('id');
            $table->text('auditores');
            $table->string('imagem_auditores');
            $table->text('inspecoes');
            $table->string('imagem_inspecoes');
            $table->text('administrador');
            $table->string('imagem_administrador');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sistema');
    }
}
