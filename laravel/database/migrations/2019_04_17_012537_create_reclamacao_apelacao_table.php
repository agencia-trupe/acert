<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReclamacaoApelacaoTable extends Migration
{
    public function up()
    {
        Schema::create('sistema_reclamacao_apelacao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sistema_reclamacao_apelacao');
    }
}
