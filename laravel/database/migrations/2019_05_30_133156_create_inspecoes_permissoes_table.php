<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspecoesPermissoesTable extends Migration
{
    public function up()
    {
        Schema::create('sistema_inspecoes_permissoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('inspecao_id')->unsigned();
            $table->foreign('inspecao_id')->references('id')->on('sistema_inspecoes')->onDelete('cascade');

            $table->integer('usuario_id');
            $table->string('usuario_type');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sistema_inspecoes_permissoes');
    }
}
