<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualidadeTable extends Migration
{
    public function up()
    {
        Schema::create('sistema_qualidade_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->timestamps();
        });

        Schema::create('sistema_qualidade_documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('arquivo');
            $table->foreign('categoria_id')->references('id')->on('sistema_qualidade_categorias')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sistema_qualidade_documentos');
        Schema::drop('sistema_qualidade_categorias');
    }
}
