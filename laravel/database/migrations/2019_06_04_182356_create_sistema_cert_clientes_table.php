<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemaCertClientesTable extends Migration
{
    public function up()
    {
        Schema::create('sistema_cert_clientes', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('nome');
            $table->integer('has_document');
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('sistema_cert_clientes');
    }
}
