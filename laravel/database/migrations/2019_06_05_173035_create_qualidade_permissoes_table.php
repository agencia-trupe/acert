<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualidadePermissoesTable extends Migration
{
    public function up()
    {
        Schema::create('sistema_qualidade_permissoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('qualidade_documento_id')->unsigned();
            $table->foreign('qualidade_documento_id')->references('id')->on('sistema_qualidade_documentos')->onDelete('cascade');

            $table->integer('usuario_id');
            $table->string('usuario_type');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sistema_qualidade_permissoes');
    }
}
