<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemaCertCertificacoesTable extends Migration
{
    public function up()
    {
        Schema::create('sistema_cert_certificacoes', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('slug');
            $table->string('cliente');
            $table->string('projeto');
            $table->text('descritivo');
            $table->string('QR_code');
            $table->string('arquivo');
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('sistema_cert_certificacoes');
    }
}
