<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspecaoTable extends Migration
{
    public function up()
    {
        Schema::create('sistema_inspecoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cliente');
            $table->string('obra');
            $table->string('os');
            $table->string('finalizado_em')->nullable();
            $table->timestamps();
        });

        Schema::create('sistema_inspecoes_documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inspecao_id')->unsigned()->nullable();
            $table->integer('auditor_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('arquivo');
            $table->text('observacoes');
            $table->foreign('inspecao_id')->references('id')->on('sistema_inspecoes')->onDelete('restrict');
            $table->foreign('auditor_id')->references('id')->on('auditoria_usuarios')->onDelete('restrict');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sistema_inspecoes_documentos');
        Schema::drop('sistema_inspecoes');
    }
}
