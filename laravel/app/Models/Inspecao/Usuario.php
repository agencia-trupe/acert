<?php

namespace App\Models\Inspecao;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Models\Sistema\Inspecao;
use App\Models\Sistema\DocumentoReclamacaoApelacao;

class Usuario extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'inspecao_usuarios';

    protected $fillable = ['nome', 'email', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function getTipoAttribute()
    {
        return 'Acesso Inspeção';
    }

    public function inspecoes()
    {
        return $this->morphToMany(
            Inspecao::class,
            'usuario',
            'sistema_inspecoes_permissoes'
        );
    }

    public function documentosReclamacaoApelacao()
    {
        return $this->morphToMany(
            DocumentoReclamacaoApelacao::class,
            'usuario',
            'sistema_reclamacao_apelacao_permissoes',
            'usuario_id',
            'reclamacao_id'
        )->ordenados();
    }
}
