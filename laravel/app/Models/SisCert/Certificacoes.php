<?php

namespace App\Models\SisCert;

use Illuminate\Database\Eloquent\Model;

class Certificacoes extends Model
{
    protected $table = 'sistema_cert_certificacoes';

    protected $guarded = ['id'];


    public function scopeOrdenados($query)
    {
        return $query->orderBy('created_at', 'DESC')->orderBy('id', 'DESC');
    }

}
