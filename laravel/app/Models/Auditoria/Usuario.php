<?php

namespace App\Models\Auditoria;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Models\Sistema\InspecaoDocumento;
use App\Models\Sistema\Inspecao;
use App\Models\Sistema\DocumentoGeral;
use App\Models\Sistema\QualidadeDocumento;

class Usuario extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'auditoria_usuarios';

    protected $fillable = ['nome', 'email', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function getTipoAttribute()
    {
        return 'Auditor de campo';
    }

    public function documentos()
    {
        return $this->hasMany(InspecaoDocumento::class, 'auditor_id')
            ->ordenados();
    }

    public function inspecoes()
    {
        return $this->morphToMany(
            Inspecao::class,
            'usuario',
            'sistema_inspecoes_permissoes'
        );
    }

    public function documentosGerais()
    {
        return $this->morphToMany(
            DocumentoGeral::class,
            'usuario',
            'sistema_documentos_gerais_permissoes'
        )->ordenados();
    }

    public function qualidadeDocumentos()
    {
        return $this->morphToMany(
            QualidadeDocumento::class,
            'usuario',
            'sistema_qualidade_permissoes'
        )->ordenados();
    }
}
