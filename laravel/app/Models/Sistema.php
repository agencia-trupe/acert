<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Sistema extends Model
{
    protected $table = 'sistema';

    protected $guarded = ['id'];

    public static function upload_imagem_auditores()
    {
        return CropImage::make('imagem_auditores', [
            'width'  => 340,
            'height' => 250,
            'path'   => 'assets/img/sistema/'
        ]);
    }

    public static function upload_imagem_inspecoes()
    {
        return CropImage::make('imagem_inspecoes', [
            'width'  => 340,
            'height' => 250,
            'path'   => 'assets/img/sistema/'
        ]);
    }

    public static function upload_imagem_administrador()
    {
        return CropImage::make('imagem_administrador', [
            'width'  => 340,
            'height' => 250,
            'path'   => 'assets/img/sistema/'
        ]);
    }

}
