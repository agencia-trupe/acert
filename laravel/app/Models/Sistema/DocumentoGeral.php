<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class DocumentoGeral extends Model
{
    protected $table = 'sistema_documentos_gerais';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function permissoes()
    {
        return $this->morphedByMany(
            \App\Models\Auditoria\Usuario::class,
            'usuario',
            'sistema_documentos_gerais_permissoes'
        );
    }
}
