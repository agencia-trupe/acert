<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class QualidadeCategoria extends Model
{
    protected $table = 'sistema_qualidade_categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function documentos()
    {
        return $this->hasMany(QualidadeDocumento::class, 'categoria_id')
            ->ordenados();
    }
}
