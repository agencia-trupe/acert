<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class Inspecao extends Model
{
    protected $table = 'sistema_inspecoes';

    protected $guarded = ['id'];

    public function documentos()
    {
        return $this->hasMany(InspecaoDocumento::class, 'inspecao_id')
            ->ordenados();
    }

    public function permissoesAuditoria()
    {
        return $this->morphedByMany(
            \App\Models\Auditoria\Usuario::class,
            'usuario',
            'sistema_inspecoes_permissoes'
        );
    }

    public function permissoesInspecao()
    {
        return $this->morphedByMany(
            \App\Models\Inspecao\Usuario::class,
            'usuario',
            'sistema_inspecoes_permissoes'
        );
    }

    public function getPermissoesAttribute()
    {
        return collect()
            ->merge($this->permissoesAuditoria)
            ->merge($this->permissoesInspecao);
    }
}
