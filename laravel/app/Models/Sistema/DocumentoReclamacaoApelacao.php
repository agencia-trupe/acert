<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class DocumentoReclamacaoApelacao extends Model
{
    protected $table = 'sistema_reclamacao_apelacao';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function permissoes()
    {
        return $this->morphedByMany(
            \App\Models\Inspecao\Usuario::class,
            'usuario',
            'sistema_reclamacao_apelacao_permissoes',
            'reclamacao_id',
            'usuario_id'
        );
    }
}
