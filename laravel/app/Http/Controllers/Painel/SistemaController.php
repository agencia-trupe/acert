<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SistemaRequest;
use App\Http\Controllers\Controller;

use App\Models\Sistema;

class SistemaController extends Controller
{
    public function index()
    {
        $registro = Sistema::first();

        return view('painel.sistema.edit', compact('registro'));
    }

    public function update(SistemaRequest $request, Sistema $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_auditores'])) $input['imagem_auditores'] = Sistema::upload_imagem_auditores();
            if (isset($input['imagem_inspecoes'])) $input['imagem_inspecoes'] = Sistema::upload_imagem_inspecoes();
            if (isset($input['imagem_administrador'])) $input['imagem_administrador'] = Sistema::upload_imagem_administrador();

            $registro->update($input);

            return redirect()->route('painel.sistema.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
