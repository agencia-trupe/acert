<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PoliticasRequest;
use App\Http\Controllers\Controller;

use App\Models\Politica;
use App\Helpers\Tools;

class PoliticasController extends Controller
{
    public function index()
    {
        $registros = Politica::ordenados()->get();

        return view('painel.politicas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.politicas.create');
    }

    public function store(PoliticasRequest $request)
    {
        try {

            $input = $request->all();

            Politica::create($input);

            return redirect()->route('painel.politicas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Politica $registro)
    {
        return view('painel.politicas.edit', compact('registro'));
    }

    public function update(PoliticasRequest $request, Politica $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.politicas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Politica $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.politicas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
