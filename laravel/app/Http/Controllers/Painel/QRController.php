<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\SisCert\Clientes;
use App\Models\SisCert\Certificacoes;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Client;

use Maatwebsite\Excel\Facades\Excel;


class QRController extends Controller
{
    public function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'N' => 'Ñ'
        );

        return $conversao;
    }

    public function index()
    {
        $certificacoes = Certificacoes::ordenados()->get();

        return view('painel.siscert.index', compact('certificacoes'));
    }


    public function busca(Request $request)
    {
        $pesquisado = $request->busca;

        $pesquisa = Certificacoes::where('projeto', 'LIKE', "%$pesquisado%")->orWhere('cliente', 'LIKE', "%$pesquisado%")->ordenados()->get();

        return view('painel.siscert.search', compact('pesquisa', 'pesquisado'));
    }


    public function qrcode()
    {
        
    }


    public function cadastro()
    {
        $clientes = Clientes::ordenados()->get();

        return view('painel.siscert.cadastro', compact('clientes'));
    }

    public function cadastroStore(Request $request)
    {
        try {
            $request->all();

            $cliente = Clientes::where('nome', $request->cliente)->firstOrFail();
            $att = 1;
            
            $cliente->update(['has_document' => $att]);
            

            $request->slug = str_replace(array(" ", "  "),'', $request->slug);

            if(substr($request->slug, -4) == '.pdf'){
                $input = $request->all();
                
                $item = Certificacoes::create($input);
            }
            else{
                $slug = $request->slug . '.pdf';

                $input = $request->all();
                $input['slug'] = $slug;

                $item = Certificacoes::create($input);
            }

            return redirect()->route('painel.siscert.cadastro2', ['id' => $item->id])->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }


    public function cadastro2($id)
    {
        $clientes = Clientes::ordenados()->get();

        $certifics = Certificacoes::where('id', $id)->first();

        return view('painel.siscert.cadastro2', compact('certifics', 'clientes'));
    }

    public function cadastroUpdate(Request $request, Certificacoes $id)
    {
        try {

            $input = $request->all();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = $id->slug;
                    $path = public_path() . '/certif';
                  
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $id->update($input);

            return redirect()->route('painel.siscert.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function cadastroDelete($id)
    {
        try {
            Certificacoes::findOrFail($id)->delete();

            return redirect()->route('painel.siscert.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }


    public function clientes()
    {
        $clientes = Clientes::ordenados()->get();

        return view('painel.siscert.clientes', compact('clientes'));
    }

    public function clientesStore(Request $request)
    {

        try {
            $input = $request->all();

            Clientes::create($input);

            return redirect()->route('painel.siscert.clientes')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function clientesDelete($id)
    {
        try {
            Clientes::find($id)->delete();

            return redirect()->route('painel.siscert.clientes')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }


    
    public function extrairIndex($pesquisado)
    {

        $exportDados = Certificacoes::where('projeto', 'LIKE', "%$pesquisado%")->orWhere('cliente', 'LIKE', "%$pesquisado%")->ordenados()->get();

        $row[] = array('Data', 'Projeto', 'Cliente', 'Descritivo');

        // $exportDados = Certificacoes::all();

        foreach ($exportDados as $dados) {
            $row[] = array(
                $dados->created_at,
                $dados->projeto,
                $dados->cliente,
                $dados->descritivo,
            );
        }

        return Excel::create('Certificados', function ($excel) use ($row) {
            $excel->setTitle('Certificados');
            $excel->sheet('Certificados', function ($sheet) use ($row) {
                $sheet->fromArray($row, null, 'A1', false, false);
            });
        })->export('csv');
    }

}
