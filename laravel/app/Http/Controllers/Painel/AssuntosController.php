<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AssuntosRequest;
use App\Http\Controllers\Controller;

use App\Models\Assunto;

class AssuntosController extends Controller
{
    public function index()
    {
        $registros = Assunto::ordenados()->get();

        return view('painel.assuntos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.assuntos.create');
    }

    public function store(AssuntosRequest $request)
    {
        try {

            $input = $request->all();

            Assunto::create($input);

            return redirect()->route('painel.assuntos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Assunto $registro)
    {
        return view('painel.assuntos.edit', compact('registro'));
    }

    public function update(AssuntosRequest $request, Assunto $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.assuntos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Assunto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.assuntos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
