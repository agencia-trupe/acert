<?php

namespace App\Http\Controllers\Inspecao\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $guard = 'inspecao';
    protected $redirectTo = 'inspecoes';
    protected $redirectAfterLogout = 'inspecoes/login';
    protected $loginView = 'inspecao.auth.login';

    public function __construct()
    {
        $this->middleware('inspecao.guest', ['except' => 'logout']);
    }
}
