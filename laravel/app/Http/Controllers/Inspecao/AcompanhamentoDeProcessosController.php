<?php

namespace App\Http\Controllers\Inspecao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\Inspecao;

class AcompanhamentoDeProcessosController extends Controller
{
    private $user;

    public function __construct(Request $request)
    {
        $route = $request->route();
        if (! $route) return;

        $inspecao   = $route->inspecao;
        $this->user = auth('inspecao')->user();

        if ($inspecao && ! $this->user->inspecoes->contains($inspecao))
            abort('404');
    }

    public function index()
    {
        $inspecoes = $this->user->inspecoes;

        return view('inspecao.acompanhamento-de-processos.index', compact('inspecoes'));
    }

    public function show(Inspecao $inspecao)
    {
        return view('inspecao.acompanhamento-de-processos.show', compact('inspecao'));
    }
}
