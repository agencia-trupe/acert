<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\Politica;

class EmpresaController extends Controller
{
    public function index($politica_slug = null)
    {
        $politica = $politica_slug
            ? Politica::whereSlug($politica_slug)->firstOrFail()
            : null;

        $politicas = Politica::ordenados()->get();
        $empresa   = Empresa::first();

        return view('frontend.empresa', compact('empresa', 'politicas', 'politica'));
    }
}
