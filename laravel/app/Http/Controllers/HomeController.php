<?php

namespace App\Http\Controllers;

use App\Models\Home;
use App\Models\Pagina;

class HomeController extends Controller
{
    public function index()
    {
        $home = Home::first();

        return view('frontend.home', compact('home'));
    }

    public function pagina($slug)
    {
        $pagina = Pagina::whereSlug($slug)->firstOrFail();

        return view('frontend.pagina', compact('pagina'));
    }
}
