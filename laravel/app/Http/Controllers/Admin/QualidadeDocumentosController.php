<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\QualidadeCategoria;
use App\Models\Sistema\QualidadeDocumento;
use App\Helpers\Tools;

class QualidadeDocumentosController extends Controller
{
    private $pathDocumento = 'assets/sistema/qualidade/';

    public function index(QualidadeCategoria $qualidadeCategoria)
    {
        return view('admin.qualidade.documentos.index', compact('qualidadeCategoria'));
    }

    public function store(Request $request, QualidadeCategoria $qualidadeCategoria)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'required|file'
        ]);

        try {
            $qualidadeCategoria->documentos()->create([
                'titulo'  => $request->titulo,
                'arquivo' => Tools::fileUpload('arquivo', $this->pathDocumento)
            ]);

            return redirect()->route('admin.qualidade.documentos.index', $qualidadeCategoria->id)->with('success', 'Documento adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar documento: '.$e->getMessage()]);
        }
    }

    public function edit(QualidadeCategoria $qualidadeCategoria, QualidadeDocumento $qualidadeDocumento)
    {
        return view('admin.qualidade.documentos.edit', compact('qualidadeCategoria', 'qualidadeDocumento'));
    }

    public function update(Request $request, QualidadeCategoria $qualidadeCategoria, QualidadeDocumento $qualidadeDocumento)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'file'
        ]);

        try {
            $input = $request->all();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', $this->pathDocumento);
            }

            $qualidadeDocumento->update($input);

            return redirect()->route('admin.qualidade.documentos.index', $qualidadeCategoria->id)->with('success', 'Documento alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar documento: '.$e->getMessage()]);
        }
    }

    public function destroy(QualidadeCategoria $qualidadeCategoria, QualidadeDocumento $qualidadeDocumento)
    {
        try {
            $qualidadeDocumento->delete();
            return redirect()->route('admin.qualidade.documentos.index', $qualidadeCategoria->id)->with('success', 'Categoria excluída com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);
        }
    }
}
