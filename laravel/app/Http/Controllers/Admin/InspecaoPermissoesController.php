<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\Inspecao;
use App\Models\Auditoria\Usuario as UsuarioAuditoria;
use App\Models\Inspecao\Usuario as UsuarioInspecao;

class InspecaoPermissoesController extends Controller
{
    public function index(Inspecao $inspecao)
    {
        $usuarios = [
            'auditor-de-campo' => UsuarioAuditoria::with('inspecoes')
                ->get()
                ->filter(function($u) use ($inspecao) {
                    return ! $u->inspecoes->contains($inspecao);
                }),
            'acesso-inspecao' => UsuarioInspecao::with('inspecoes')
                ->get()
                ->filter(function($u) use ($inspecao) {
                    return ! $u->inspecoes->contains($inspecao);
                })
        ];

        return view('admin.inspecao.permissoes', compact('inspecao', 'usuarios'));
    }

    public function store(Request $request, Inspecao $inspecao)
    {
        $this->validate($request, [
            'perfil' => 'required|in:auditor-de-campo,acesso-inspecao',
            'usuario' => 'required|integer'
        ]);

        switch ($request->perfil) {
            case 'auditor-de-campo':
                $usuario = UsuarioAuditoria::findOrFail($request->usuario);
                break;
            case 'acesso-inspecao':
                $usuario = UsuarioInspecao::findOrFail($request->usuario);
                break;
            default:
                abort('404');
        }

        try {
            if ($usuario->inspecoes->contains($inspecao)) {
                throw new \Exception('O usuário já possui permissão para acessar essa inspeção.');
            }
            $usuario->inspecoes()->save($inspecao);
            return redirect()->route('admin.inspecao.permissoes', $inspecao->id)->with('success', 'Permissão criada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao criar permissão: '.$e->getMessage()]);
        }
    }

    public function destroy(Inspecao $inspecao, $tipo, $id)
    {
        switch ($tipo) {
            case 'auditor-de-campo':
                $usuario = UsuarioAuditoria::findOrFail($id);
                break;
            case 'acesso-inspecao':
                $usuario = UsuarioInspecao::findOrFail($id);
                break;
            default:
                abort('404');
        }

        try {
            $usuario->inspecoes()->detach($inspecao);
            return redirect()->route('admin.inspecao.permissoes', $inspecao->id)->with('success', 'Permissão excluída com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir permissão: '.$e->getMessage()]);
        }
    }
}
