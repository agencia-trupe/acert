<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\QualidadeCategoria;

class QualidadeController extends Controller
{
    public function index()
    {
        $categorias = QualidadeCategoria::ordenados()->get();

        return view('admin.qualidade.index', compact('categorias'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo'  => 'required',
        ]);

        try {
            QualidadeCategoria::create([
                'titulo'  => $request->titulo,
            ]);

            return redirect()->route('admin.qualidade.index')->with('success', 'Categoria adicionada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);
        }
    }

    public function edit(QualidadeCategoria $qualidadeCategoria)
    {
        return view('admin.qualidade.edit', compact('qualidadeCategoria'));
    }

    public function update(Request $request, QualidadeCategoria $qualidadeCategoria)
    {
        $this->validate($request, [
            'titulo'  => 'required',
        ]);

        try {
            $qualidadeCategoria->update([
                'titulo'  => $request->titulo,
            ]);

            return redirect()->route('admin.qualidade.index')->with('success', 'Categoria alterada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);
        }
    }

    public function destroy(QualidadeCategoria $qualidadeCategoria)
    {
        try {
            $qualidadeCategoria->delete();
            return redirect()->route('admin.qualidade.index')->with('success', 'Categoria excluída com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);
        }
    }
}
