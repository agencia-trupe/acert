<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\DocumentoGeral;
use App\Models\Auditoria\Usuario as UsuarioAuditoria;

class DocumentosGeraisPermissoesController extends Controller
{
    public function index(DocumentoGeral $documentoGeral)
    {
        $usuarios = UsuarioAuditoria::with('documentosGerais')
            ->get()
            ->filter(function($u) use ($documentoGeral) {
                return ! $u->documentosGerais->contains($documentoGeral);
            });

        return view('admin.documentos-gerais.permissoes', compact('documentoGeral', 'usuarios'));
    }

    public function store(Request $request, DocumentoGeral $documentoGeral)
    {
        $this->validate($request, [
            'perfil' => 'required|in:auditor-de-campo',
            'usuario' => 'required|integer'
        ]);

        switch ($request->perfil) {
            case 'auditor-de-campo':
                $usuario = UsuarioAuditoria::findOrFail($request->usuario);
                break;
            default:
                abort('404');
        }

        try {
            if ($usuario->documentosGerais->contains($documentoGeral)) {
                throw new \Exception('O usuário já possui permissão para acessar esse documento.');
            }
            $usuario->documentosGerais()->save($documentoGeral);
            return redirect()->route('admin.documentosGerais.permissoes', $documentoGeral->id)->with('success', 'Permissão criada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao criar permissão: '.$e->getMessage()]);
        }
    }

    public function destroy(DocumentoGeral $documentoGeral, $tipo, $id)
    {
        switch ($tipo) {
            case 'auditor-de-campo':
                $usuario = UsuarioAuditoria::findOrFail($id);
                break;
            default:
                abort('404');
        }

        try {
            $usuario->documentosGerais()->detach($documentoGeral);
            return redirect()->route('admin.documentosGerais.permissoes', $documentoGeral->id)->with('success', 'Permissão excluída com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir permissão: '.$e->getMessage()]);
        }
    }
}
