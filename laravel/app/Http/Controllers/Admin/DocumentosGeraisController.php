<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\DocumentoGeral;
use App\Helpers\Tools;

class DocumentosGeraisController extends Controller
{
    private $pathDocumento = 'assets/sistema/documentos-gerais/';

    public function index()
    {
        $documentos = DocumentoGeral::ordenados()->get();

        return view('admin.documentos-gerais.index', compact('documentos'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'required|file'
        ]);

        try {
            DocumentoGeral::create([
                'titulo'  => $request->titulo,
                'arquivo' => Tools::fileUpload('arquivo', $this->pathDocumento)
            ]);

            return redirect()->route('admin.documentosGerais.index')->with('success', 'Documento adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar documento: '.$e->getMessage()]);
        }
    }

    public function edit(DocumentoGeral $documentoGeral)
    {
        return view('admin.documentos-gerais.edit', compact('documentoGeral'));
    }

    public function update(Request $request, DocumentoGeral $documentoGeral)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'file'
        ]);

        try {
            $input = $request->all();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', $this->pathDocumento);
            }

            $documentoGeral->update($input);

            return redirect()->route('admin.documentosGerais.index')->with('success', 'Documento alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar documento: '.$e->getMessage()]);
        }
    }

    public function destroy(DocumentoGeral $documentoGeral)
    {
        try {
            $documentoGeral->delete();
            return redirect()->route('admin.documentosGerais.index')->with('success', 'Documento excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir documento: '.$e->getMessage()]);
        }
    }
}
