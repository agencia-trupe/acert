<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\DocumentoReclamacaoApelacao;
use App\Helpers\Tools;

class ReclamacaoApelacaoController extends Controller
{
    private $pathDocumento = 'assets/sistema/reclamacao-apelacao/';

    public function index()
    {
        $documentos = DocumentoReclamacaoApelacao::ordenados()->get();

        return view('admin.reclamacao-apelacao.index', compact('documentos'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'required|file'
        ]);

        try {
            DocumentoReclamacaoApelacao::create([
                'titulo'  => $request->titulo,
                'arquivo' => Tools::fileUpload('arquivo', $this->pathDocumento)
            ]);

            return redirect()->route('admin.reclamacaoApelacao.index')->with('success', 'Documento adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar documento: '.$e->getMessage()]);
        }
    }

    public function edit(DocumentoReclamacaoApelacao $documentoReclamacaoApelacao)
    {
        return view('admin.reclamacao-apelacao.edit', compact('documentoReclamacaoApelacao'));
    }

    public function update(Request $request, DocumentoReclamacaoApelacao $documentoReclamacaoApelacao)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'file'
        ]);

        try {
            $input = $request->all();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', $this->pathDocumento);
            }

            $documentoReclamacaoApelacao->update($input);

            return redirect()->route('admin.reclamacaoApelacao.index')->with('success', 'Documento alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar documento: '.$e->getMessage()]);
        }
    }

    public function destroy(DocumentoReclamacaoApelacao $documentoReclamacaoApelacao)
    {
        try {
            $documentoReclamacaoApelacao->delete();
            return redirect()->route('admin.reclamacaoApelacao.index')->with('success', 'Documento excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir documento: '.$e->getMessage()]);
        }
    }
}
