<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\DocumentoReclamacaoApelacao;
use App\Models\Inspecao\Usuario as UsuarioInspecao;

class ReclamacaoApelacaoPermissoesController extends Controller
{
    public function index(DocumentoReclamacaoApelacao $documentoReclamacaoApelacao)
    {
        $usuarios = UsuarioInspecao::with('documentosReclamacaoApelacao')
            ->get()
            ->filter(function($u) use ($documentoReclamacaoApelacao) {
                return ! $u->documentosReclamacaoApelacao->contains($documentoReclamacaoApelacao);
            });

        return view('admin.reclamacao-apelacao.permissoes', compact('documentoReclamacaoApelacao', 'usuarios'));
    }

    public function store(Request $request, DocumentoReclamacaoApelacao $documentoReclamacaoApelacao)
    {
        $this->validate($request, [
            'perfil' => 'required|in:acesso-inspecao',
            'usuario' => 'required|integer'
        ]);

        switch ($request->perfil) {
            case 'acesso-inspecao':
                $usuario = UsuarioInspecao::findOrFail($request->usuario);
                break;
            default:
                abort('404');
        }

        try {
            if ($usuario->documentosReclamacaoApelacao->contains($documentoReclamacaoApelacao)) {
                throw new \Exception('O usuário já possui permissão para acessar esse documento.');
            }
            $usuario->documentosReclamacaoApelacao()->save($documentoReclamacaoApelacao);
            return redirect()->route('admin.reclamacaoApelacao.permissoes', $documentoReclamacaoApelacao->id)->with('success', 'Permissão criada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao criar permissão: '.$e->getMessage()]);
        }
    }

    public function destroy(DocumentoReclamacaoApelacao $documentoReclamacaoApelacao, $tipo, $id)
    {
        switch ($tipo) {
            case 'acesso-inspecao':
                $usuario = UsuarioInspecao::findOrFail($id);
                break;
            default:
                abort('404');
        }

        try {
            $usuario->documentosReclamacaoApelacao()->detach($documentoReclamacaoApelacao);
            return redirect()->route('admin.reclamacaoApelacao.permissoes', $documentoReclamacaoApelacao->id)->with('success', 'Permissão excluída com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir permissão: '.$e->getMessage()]);
        }
    }
}
