<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\QualidadeCategoria;
use App\Models\Sistema\QualidadeDocumento;
use App\Models\Auditoria\Usuario as UsuarioAuditoria;

class QualidadeDocumentosPermissoesController extends Controller
{
    public function index(QualidadeCategoria $qualidadeCategoria, QualidadeDocumento $qualidadeDocumento)
    {
        $usuarios = UsuarioAuditoria::with('qualidadeDocumentos')
            ->get()
            ->filter(function($u) use ($qualidadeDocumento) {
                return ! $u->qualidadeDocumentos->contains($qualidadeDocumento);
            });

        return view('admin.qualidade.documentos.permissoes', compact('qualidadeCategoria', 'qualidadeDocumento', 'usuarios'));
    }

    public function store(Request $request, QualidadeCategoria $qualidadeCategoria, QualidadeDocumento $qualidadeDocumento)
    {
        $this->validate($request, [
            'perfil' => 'required|in:auditor-de-campo',
            'usuario' => 'required|integer'
        ]);

        switch ($request->perfil) {
            case 'auditor-de-campo':
                $usuario = UsuarioAuditoria::findOrFail($request->usuario);
                break;
            default:
                abort('404');
        }

        try {
            if ($usuario->qualidadeDocumentos->contains($qualidadeDocumento)) {
                throw new \Exception('O usuário já possui permissão para acessar esse documento.');
            }
            $usuario->qualidadeDocumentos()->save($qualidadeDocumento);
            return redirect()->route('admin.qualidade.documentos.permissoes', [ $qualidadeCategoria->id, $qualidadeDocumento->id])->with('success', 'Permissão criada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao criar permissão: '.$e->getMessage()]);
        }
    }

    public function destroy(QualidadeCategoria $qualidadeCategoria, QualidadeDocumento $qualidadeDocumento, $tipo, $id)
    {
        switch ($tipo) {
            case 'auditor-de-campo':
                $usuario = UsuarioAuditoria::findOrFail($id);
                break;
            default:
                abort('404');
        }

        try {
            $usuario->qualidadeDocumentos()->detach($qualidadeDocumento);
            return redirect()->route('admin.qualidade.documentos.permissoes', [$qualidadeCategoria->id, $qualidadeDocumento->id])->with('success', 'Permissão excluída com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir permissão: '.$e->getMessage()]);
        }
    }
}
