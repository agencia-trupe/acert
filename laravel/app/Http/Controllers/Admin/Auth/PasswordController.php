<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    use ResetsPasswords;

    protected $broker = 'admin';
    protected $redirectPath = 'admin/login';
    protected $linkRequestView = 'admin.auth.password';
    protected $resetView = 'admin.auth.reset';

    public function __construct()
    {
        $this->middleware('admin.guest');
    }
}
