<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $guard = 'admin';
    protected $redirectTo = 'admin';
    protected $redirectAfterLogout = 'admin/login';
    protected $loginView = 'admin.auth.login';

    public function __construct()
    {
        $this->middleware('admin.guest', ['except' => 'logout']);
    }
}
