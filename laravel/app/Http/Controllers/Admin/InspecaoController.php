<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\Inspecao;
use Carbon\Carbon;

class InspecaoController extends Controller
{
    public function index()
    {
        $inspecoes = new Inspecao;

        if ($cliente = request('cliente')) {
            $inspecoes = $inspecoes->where('cliente', 'LIKE', '%'.$cliente.'%');
        }
        if ($obra = request('obra')) {
            $inspecoes = $inspecoes->where('obra', 'LIKE', '%'.$obra.'%');
        }
        if ($os = request('os')) {
            $inspecoes = $inspecoes->where('os', 'LIKE', '%'.$os.'%');
        }
        if (request('ordem') && request('dir')) {
            $inspecoes = $inspecoes->orderBy(request('ordem'), request('dir'));
        }

        $inspecoes = $inspecoes->get();

        return view('admin.inspecao.index', compact('inspecoes'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'cliente' => 'required',
            'obra'    => 'required',
            'os'      => 'required',
        ]);

        try {
            Inspecao::create([
                'cliente' => $request->cliente,
                'obra'    => $request->obra,
                'os'      => $request->os,
            ]);

            return redirect()->route('admin.inspecao.index')->with('success', 'Inspeção adicionada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar inspeção: '.$e->getMessage()]);
        }
    }

    public function edit(Inspecao $inspecao)
    {
        return view('admin.inspecao.edit', compact('inspecao'));
    }

    public function update(Request $request, Inspecao $inspecao)
    {
        $this->validate($request, [
            'cliente' => 'required',
            'obra'    => 'required',
            'os'      => 'required',
        ]);

        try {
            $inspecao->update([
                'cliente' => $request->cliente,
                'obra'    => $request->obra,
                'os'      => $request->os,
            ]);

            return redirect()->route('admin.inspecao.index')->with('success', 'Inspeção alterada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar inspeção: '.$e->getMessage()]);
        }
    }

    public function destroy(Inspecao $inspecao)
    {
        try {
            if ($inspecao->documentos->count()) {
                throw new \Exception('Não é possível excluir inspeções que possuem documentos cadastrados.');
            }
            $inspecao->delete();
            return redirect()->route('admin.inspecao.index')->with('success', 'Inspeção excluída com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir inspeção: '.$e->getMessage()]);
        }
    }

    public function documentos(Inspecao $inspecao)
    {
        return view('admin.inspecao.documentos', compact('inspecao'));
    }

    public function finalizar(Inspecao $inspecao)
    {
        try {
            if (! count($inspecao->documentos)) {
                throw new \Exception('A inspeção não possui nenhum documento.');
            }
            $inspecao->update([
                'finalizado_em' => Carbon::now()
            ]);
            return redirect()->route('admin.inspecao.documentos', $inspecao->id)->with('success', 'Inspeção finalizada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao finalizar inspeção: '.$e->getMessage()]);
        }
    }

    public function reabrir(Inspecao $inspecao)
    {
        try {
            $inspecao->update([
                'finalizado_em' => null
            ]);
            return redirect()->route('admin.inspecao.documentos', $inspecao->id)->with('success', 'Inspeção reaberta com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao reabrir inspeção: '.$e->getMessage()]);
        }
    }
}
