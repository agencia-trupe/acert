<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function order(Request $request)
    {
        if (! $request->ajax()) abort('404');

        $data  = $request->input('data');
        $table = $request->input('table');

        for ($i = 0; $i < count($data); $i++) {
            \DB::table($table)
                ->where('id', $data[$i])
                ->update(array('ordem' => $i + 1));
        }

        return json_encode($data);
    }
}
