<?php

namespace App\Http\Controllers\Auditoria;

use App\Http\Controllers\Controller;

class DocumentosGeraisController extends Controller
{
    public function index()
    {
        $documentos = auth('auditoria')->user()->documentosGerais;

        return view('auditoria.documentos-gerais', compact('documentos'));
    }
}
