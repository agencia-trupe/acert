<?php

namespace App\Http\Controllers\Auditoria;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sistema\Inspecao;
use App\Models\Sistema\InspecaoDocumento;

use App\Helpers\Tools;
use Carbon\Carbon;

class InspecaoController extends Controller
{
    private $pathDocumento = 'assets/sistema/inspecao/';
    private $user;

    public function __construct(Request $request)
    {
        $route = $request->route();
        if (! $route) return;

        $inspecao   = $route->inspecao;
        $this->user = auth('auditoria')->user();

        if ($inspecao && ! $this->user->inspecoes->contains($inspecao))
            abort('404');
    }

    public function index()
    {
        $inspecoes   = $this->user->inspecoes;
        $emProcesso  = $inspecoes->filter(function($i) {
            return $i->finalizado_em == null;
        });
        $finalizados = $inspecoes->filter(function($i) {
            return $i->finalizado_em != null;
        });

        return view('auditoria.inspecao.index', compact('emProcesso', 'finalizados'));
    }

    public function show(Inspecao $inspecao)
    {
        return view('auditoria.inspecao.show', compact('inspecao'));
    }

    public function create(Inspecao $inspecao)
    {
        return view('auditoria.inspecao.create', compact('inspecao'));
    }

    public function store(Request $request, Inspecao $inspecao)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'required|file'
        ]);

        try {
            if ($inspecao->finalizado_em) {
                throw new \Exception('A inspeção já foi finalizada.');
            }
            $inspecao->documentos()->create([
                'auditor_id'  => $this->user->id,
                'titulo'      => $request->titulo,
                'arquivo'     => Tools::fileUpload('arquivo', $this->pathDocumento),
                'observacoes' => $request->observacoes
            ]);

            return redirect()->route('auditoria.inspecao.show', $inspecao->id)->with('success', 'Documento adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar documento: '.$e->getMessage()]);
        }
    }

    public function edit(Inspecao $inspecao, InspecaoDocumento $inspecaoDocumento)
    {
        return view('auditoria.inspecao.edit', compact('inspecao', 'inspecaoDocumento'));
    }

    public function update(Request $request, Inspecao $inspecao, InspecaoDocumento $inspecaoDocumento)
    {
        $this->validate($request, [
            'titulo'  => 'required',
            'arquivo' => 'file'
        ]);

        try {
            if ($inspecao->finalizado_em) {
                throw new \Exception('A inspeção já foi finalizada.');
            }
            $inspecaoDocumento->update([
                'auditor_id'  => $this->user->id,
                'titulo'      => $request->titulo,
                'arquivo'     => $request->hasFile('arquivo')
                    ? Tools::fileUpload('arquivo', $this->pathDocumento)
                    : $inspecaoDocumento->arquivo,
                'observacoes' => $request->observacoes
            ]);

            return redirect()->route('auditoria.inspecao.show', $inspecao->id)->with('success', 'Documento alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar documento: '.$e->getMessage()]);
        }
    }

    public function destroy(Inspecao $inspecao, InspecaoDocumento $inspecaoDocumento) {
        try {
            if ($inspecao->finalizado_em) {
                throw new \Exception('A inspeção já foi finalizada.');
            }
            $inspecaoDocumento->delete();
            return redirect()->route('auditoria.inspecao.show', $inspecao->id)->with('success', 'Documento excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir documento: '.$e->getMessage()]);
        }
    }

    public function finalizar(Inspecao $inspecao)
    {
        try {
            if (! count($inspecao->documentos)) {
                throw new \Exception('A inspeção não possui nenhum documento.');
            }
            $inspecao->update([
                'finalizado_em' => Carbon::now()
            ]);
            return redirect()->route('auditoria.inspecao.show', $inspecao->id)->with('success', 'Inspeção finalizada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao finalizar inspeção: '.$e->getMessage()]);
        }
    }

    public function order(Request $request, Inspecao $inspecao)
    {
        if (! $request->ajax()) abort('404');
        if ($inspecao->finalizado_em) abort('500');

        $data  = $request->input('data');
        $table = $request->input('table');

        for ($i = 0; $i < count($data); $i++) {
            \DB::table($table)
                ->where('id', $data[$i])
                ->update(array('ordem' => $i + 1));
        }

        return json_encode($data);
    }
}
