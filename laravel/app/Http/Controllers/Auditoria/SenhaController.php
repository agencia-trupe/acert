<?php

namespace App\Http\Controllers\Auditoria;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SenhaController extends Controller
{
    public function index()
    {
        return view('auditoria.senha.index');
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'password'   => 'required',
            'nova_senha' => 'required|confirmed|min:6'
        ]);

        $user = auth('auditoria')->user();

        if (! Hash::check($request->password, $user->password)) {
            return back()->withErrors('Senha inválida.');
        }

        $user->update([
            'password' => bcrypt($request->nova_senha)
        ]);

        return back()->with('success', 'Senha alterada com sucesso!');
    }
}
