<?php

namespace App\Http\Controllers\Auditoria\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $guard = 'auditoria';
    protected $redirectTo = 'auditores';
    protected $redirectAfterLogout = 'auditores/login';
    protected $loginView = 'auditoria.auth.login';

    public function __construct()
    {
        $this->middleware('auditoria.guest', ['except' => 'logout']);
    }
}
