<?php

namespace App\Http\Controllers\Auditoria;

use App\Http\Controllers\Controller;

use App\Models\Sistema\QualidadeCategoria;

class QualidadeController extends Controller
{
    public function index()
    {
        $user = auth('auditoria')->user();
        $categorias = QualidadeCategoria::with('documentos')
            ->ordenados()
            ->get()
            ->map(function($categoria) use ($user) {
                $categoria->documentos = $categoria->documentos->filter(function($documento) use ($user) {
                    return $user->qualidadeDocumentos->contains($documento);
                });
                return $categoria;
            })
            ->filter(function($categoria) {
                return !!$categoria->documentos->count();
            });

        return view('auditoria.qualidade', compact('categorias'));
    }
}
