<?php

namespace App\Http\Controllers;

use App\Models\Sistema;

class SistemaController extends Controller
{
    public function index()
    {
        $sistema = Sistema::first();

        return view('frontend.sistema', compact('sistema'));
    }
}
