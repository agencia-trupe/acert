<?php

Route::group([
    'prefix'     => 'painel',
    'namespace'  => 'Painel',
    'middleware' => ['auth']
], function() {
    Route::get('/', 'PainelController@index')->name('painel');

    /* GENERATED ROUTES */
		Route::resource('sistema', 'SistemaController', ['only' => ['index', 'update']]);
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('assuntos', 'AssuntosController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
    Route::resource('paginas', 'PaginasController');
    Route::resource('politicas', 'PoliticasController');
    Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

    Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController');
    Route::resource('usuarios', 'UsuariosController');

    Route::post('ckeditor-upload', 'PainelController@imageUpload');
    Route::post('order', 'PainelController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::get('generator', 'GeneratorController@index')->name('generator.index');
    Route::post('generator', 'GeneratorController@submit')->name('generator.submit');


    //Sistema Certificação
    Route::get('sistema-cert', 'QRController@index')->name('painel.siscert.index');
    Route::get('cadastro', 'QRController@cadastro')->name('painel.siscert.cadastro');
    Route::get('cadastro-parte2/{id}', 'QRController@cadastro2')->name('painel.siscert.cadastro2');
    Route::get('clientes', 'QRController@clientes')->name('painel.siscert.clientes');
    Route::get('certificados/{pesquisado}', 'QRController@extrairIndex')->name('painel.siscert.certs');

    Route::post('clientes/store', 'QRController@clientesStore')->name('painel.siscert.store');
    Route::post('cadastro/store', 'QRController@cadastroStore')->name('painel.siscert.cstore');
    Route::post('search', 'QRController@busca')->name('painel.siscert.busca');

    Route::put('cadastro/update/{id}', 'QRController@cadastroUpdate')->name('painel.siscert.update');

    Route::delete('clientes/delete/{id}', 'QRController@clientesDelete')->name('painel.siscert.delete');
    Route::delete('cadastro/delete/{id}', 'QRController@cadastroDelete')->name('painel.siscert.cdelete');

    Route::get('qrcode', 'QRController@qrcode')->name('painel.siscert.qrcode');
});

// Auth
Route::group([
    'prefix'    => 'painel',
    'namespace' => 'Auth'
], function() {
    Route::get('login', 'AuthController@showLoginForm')->name('auth');
    Route::post('login', 'AuthController@login')->name('login');
    Route::get('logout', 'AuthController@logout')->name('logout');
});
