<?php

Route::group([
    'prefix' => 'auditores',
    'namespace' => 'Auditoria\Auth'
], function() {
    Route::get('login', 'AuthController@showLoginForm')
        ->name('auditoria.login');
    Route::post('login', 'AuthController@login')
        ->name('auditoria.login');
    Route::get('logout', 'AuthController@logout')
        ->name('auditoria.logout');
    Route::get('esqueci-minha-senha', 'PasswordController@showLinkRequestForm')
        ->name('auditoria.password.linkRequest');
    Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')
        ->name('auditoria.password.sendResetLink');
    Route::get('redefinir-senha/{token}', 'PasswordController@showResetForm')
        ->name('auditoria.password.resetForm');
    Route::post('redefinir-senha', 'PasswordController@reset')
        ->name('auditoria.password.reset');
});

Route::group([
    'prefix'     => 'auditores',
    'namespace'  => 'Auditoria',
    'middleware' => ['auditoria.auth']
], function() {
    Route::get('/', function() {
        return redirect()->route('auditoria.documentosGerais');
    })->name('auditoria');

    Route::get('documentos-gerais', 'DocumentosGeraisController@index')
        ->name('auditoria.documentosGerais');

    Route::get('qualidade', 'QualidadeController@index')
        ->name('auditoria.qualidade');

    Route::get('inspecao', 'InspecaoController@index')
        ->name('auditoria.inspecao');
    Route::get('inspecao/{inspecao}', 'InspecaoController@show')
        ->name('auditoria.inspecao.show');
    Route::get('inspecao/{inspecao}/finalizar', 'InspecaoController@finalizar')
        ->name('auditoria.inspecao.finalizar');
    Route::get('inspecao/{inspecao}/adicionar-documento', 'InspecaoController@create')
        ->name('auditoria.inspecao.create');
    Route::post('inspecao/{inspecao}/adicionar-documento', 'InspecaoController@store')
        ->name('auditoria.inspecao.store');
    Route::get('inspecao/{inspecao}/documento/{inspecaoDocumento}/edit', 'InspecaoController@edit')
        ->name('auditoria.inspecao.edit');
    Route::patch('inspecao/{inspecao}/documento/{inspecaoDocumento}/update', 'InspecaoController@update')
        ->name('auditoria.inspecao.update');
    Route::get('inspecao/{inspecao}/documento/{inspecaoDocumento}/destroy', 'InspecaoController@destroy')
        ->name('auditoria.inspecao.destroy');
    Route::post('inspecao/{inspecao}/order', 'InspecaoController@order')
        ->name('auditoria.inspecao.order');

    Route::get('alterar-senha', 'SenhaController@index')
        ->name('auditoria.alterarSenha');
    Route::post('alterar-senha', 'SenhaController@post')
        ->name('auditoria.alterarSenha.post');
});
