<?php

Route::group([
    'prefix' => 'inspecoes',
    'namespace' => 'Inspecao\Auth'
], function() {
    Route::get('login', 'AuthController@showLoginForm')
        ->name('inspecao.login');
    Route::post('login', 'AuthController@login')
        ->name('inspecao.login');
    Route::get('logout', 'AuthController@logout')
        ->name('inspecao.logout');
    Route::get('esqueci-minha-senha', 'PasswordController@showLinkRequestForm')
        ->name('inspecao.password.linkRequest');
    Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')
        ->name('inspecao.password.sendResetLink');
    Route::get('redefinir-senha/{token}', 'PasswordController@showResetForm')
        ->name('inspecao.password.resetForm');
    Route::post('redefinir-senha', 'PasswordController@reset')
        ->name('inspecao.password.reset');
});

Route::group([
    'prefix'     => 'inspecoes',
    'namespace'  => 'Inspecao',
    'middleware' => ['inspecao.auth']
], function() {
    Route::get('/', function() {
        return redirect()->route('inspecao.acompanhamentoDeProcessos');
    })->name('inspecao');

    Route::get('acompanhamento-de-processos', 'AcompanhamentoDeProcessosController@index')
        ->name('inspecao.acompanhamentoDeProcessos');
    Route::get('acompanhamento-de-processos/{inspecao}', 'AcompanhamentoDeProcessosController@show')
        ->name('inspecao.acompanhamentoDeProcessos.show');

    Route::get('reclamacao-e-apelacao', 'ReclamacaoApelacaoController@index')
        ->name('inspecao.reclamacaoApelacao');

    Route::get('alterar-senha', 'SenhaController@index')
        ->name('inspecao.alterarSenha');
    Route::post('alterar-senha', 'SenhaController@post')
        ->name('inspecao.alterarSenha.post');
});
