<?php

Route::get('/', 'HomeController@index')
    ->name('home');

Route::get('empresa/{politica_slug?}', 'EmpresaController@index')
    ->name('empresa');

Route::get('contato', 'ContatoController@index')
    ->name('contato');
Route::post('contato', 'ContatoController@post')
    ->name('contato.post');

Route::get('sistema', 'SistemaController@index')
    ->name('sistema');

Route::get('servicos/{pagina}', 'HomeController@pagina')
    ->name('pagina');
