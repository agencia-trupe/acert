<?php

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin\Auth'
], function() {
    Route::get('login', 'AuthController@showLoginForm')
        ->name('admin.login');
    Route::post('login', 'AuthController@login')
        ->name('admin.login');
    Route::get('logout', 'AuthController@logout')
        ->name('admin.logout');
    Route::get('esqueci-minha-senha', 'PasswordController@showLinkRequestForm')
        ->name('admin.password.linkRequest');
    Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')
        ->name('admin.password.sendResetLink');
    Route::get('redefinir-senha/{token}', 'PasswordController@showResetForm')
        ->name('admin.password.resetForm');
    Route::post('redefinir-senha', 'PasswordController@reset')
        ->name('admin.password.reset');
});

Route::group([
    'prefix'     => 'admin',
    'namespace'  => 'Admin',
    'middleware' => ['admin.auth']
], function() {
    Route::get('/', function() {
        return redirect()->route('admin.documentosGerais.index');
    })->name('admin');

    Route::get('documentos-gerais', 'DocumentosGeraisController@index')
        ->name('admin.documentosGerais.index');
    Route::post('documentos-gerais/store', 'DocumentosGeraisController@store')
        ->name('admin.documentosGerais.store');
    Route::get('documentos-gerais/{documentoGeral}/edit', 'DocumentosGeraisController@edit')
        ->name('admin.documentosGerais.edit');
    Route::patch('documentos-gerais/{documentoGeral}/update', 'DocumentosGeraisController@update')
        ->name('admin.documentosGerais.update');
    Route::get('documentos-gerais/{documentoGeral}/destroy', 'DocumentosGeraisController@destroy')
        ->name('admin.documentosGerais.destroy');
    Route::get('documentos-gerais/{documentoGeral}/permissoes', 'DocumentosGeraisPermissoesController@index')
        ->name('admin.documentosGerais.permissoes');
    Route::post('documentos-gerais/{documentoGeral}/permissoes', 'DocumentosGeraisPermissoesController@store')
        ->name('admin.documentosGerais.permissoes.store');
    Route::get('documentos-gerais/{documentoGeral}/permissoes/{tipo}/{id}/destroy', 'DocumentosGeraisPermissoesController@destroy')
        ->name('admin.documentosGerais.permissoes.destroy');

    Route::get('qualidade', 'QualidadeController@index')
        ->name('admin.qualidade.index');
    Route::post('qualidade/store', 'QualidadeController@store')
        ->name('admin.qualidade.store');
    Route::get('qualidade/{qualidadeCategoria}/edit', 'QualidadeController@edit')
        ->name('admin.qualidade.edit');
    Route::patch('qualidade/{qualidadeCategoria}/update', 'QualidadeController@update')
        ->name('admin.qualidade.update');
    Route::get('qualidade/{qualidadeCategoria}/destroy', 'QualidadeController@destroy')
        ->name('admin.qualidade.destroy');
    Route::get('qualidade/{qualidadeCategoria}/documentos', 'QualidadeDocumentosController@index')
        ->name('admin.qualidade.documentos.index');
    Route::post('qualidade/{qualidadeCategoria}/documentos/store', 'QualidadeDocumentosController@store')
        ->name('admin.qualidade.documentos.store');
    Route::get('qualidade/{qualidadeCategoria}/documentos/{qualidadeDocumento}/edit', 'QualidadeDocumentosController@edit')
        ->name('admin.qualidade.documentos.edit');
    Route::patch('qualidade/{qualidadeCategoria}/documentos/{qualidadeDocumento}/update', 'QualidadeDocumentosController@update')
        ->name('admin.qualidade.documentos.update');
    Route::get('qualidade/{qualidadeCategoria}/documentos/{qualidadeDocumento}/destroy', 'QualidadeDocumentosController@destroy')
        ->name('admin.qualidade.documentos.destroy');
    Route::get('qualidade/{qualidadeCategoria}/documentos/{qualidadeDocumento}/permissoes', 'QualidadeDocumentosPermissoesController@index')
        ->name('admin.qualidade.documentos.permissoes');
    Route::post('qualidade/{qualidadeCategoria}/documentos/{qualidadeDocumento}/permissoes', 'QualidadeDocumentosPermissoesController@store')
        ->name('admin.qualidade.documentos.permissoes.store');
    Route::get('qualidade/{qualidadeCategoria}/documentos/{qualidadeDocumento}/permissoes/{tipo}/{id}/destroy', 'QualidadeDocumentosPermissoesController@destroy')
        ->name('admin.qualidade.documentos.permissoes.destroy');

    Route::get('inspecao', 'InspecaoController@index')
        ->name('admin.inspecao.index');
    Route::post('inspecao/store', 'InspecaoController@store')
        ->name('admin.inspecao.store');
    Route::get('inspecao/{inspecao}/edit', 'InspecaoController@edit')
        ->name('admin.inspecao.edit');
    Route::patch('inspecao/{inspecao}/update', 'InspecaoController@update')
        ->name('admin.inspecao.update');
    Route::get('inspecao/{inspecao}/destroy', 'InspecaoController@destroy')
        ->name('admin.inspecao.destroy');
    Route::get('inspecao/{inspecao}/documentos', 'InspecaoController@documentos')
        ->name('admin.inspecao.documentos');
    Route::get('inspecao/{inspecao}/finalizar', 'InspecaoController@finalizar')
        ->name('admin.inspecao.finalizar');
    Route::get('inspecao/{inspecao}/reabrir', 'InspecaoController@reabrir')
        ->name('admin.inspecao.reabrir');
    Route::get('inspecao/{inspecao}/permissoes', 'InspecaoPermissoesController@index')
        ->name('admin.inspecao.permissoes');
    Route::post('inspecao/{inspecao}/permissoes', 'InspecaoPermissoesController@store')
        ->name('admin.inspecao.permissoes.store');
    Route::get('inspecao/{inspecao}/permissoes/{tipo}/{id}/destroy', 'InspecaoPermissoesController@destroy')
        ->name('admin.inspecao.permissoes.destroy');

    Route::get('reclamacao-e-apelacao', 'ReclamacaoApelacaoController@index')
        ->name('admin.reclamacaoApelacao.index');
    Route::post('reclamacao-e-apelacao/store', 'ReclamacaoApelacaoController@store')
        ->name('admin.reclamacaoApelacao.store');
    Route::get('reclamacao-e-apelacao/{documentoReclamacaoApelacao}/edit', 'ReclamacaoApelacaoController@edit')
        ->name('admin.reclamacaoApelacao.edit');
    Route::patch('reclamacao-e-apelacao/{documentoReclamacaoApelacao}/update', 'ReclamacaoApelacaoController@update')
        ->name('admin.reclamacaoApelacao.update');
    Route::get('reclamacao-e-apelacao/{documentoReclamacaoApelacao}/destroy', 'ReclamacaoApelacaoController@destroy')
        ->name('admin.reclamacaoApelacao.destroy');
    Route::get('reclamacao-e-apelacao/{documentoReclamacaoApelacao}/permissoes', 'ReclamacaoApelacaoPermissoesController@index')
        ->name('admin.reclamacaoApelacao.permissoes');
    Route::post('reclamacao-e-apelacao/{documentoReclamacaoApelacao}/permissoes', 'ReclamacaoApelacaoPermissoesController@store')
        ->name('admin.reclamacaoApelacao.permissoes.store');
    Route::get('reclamacao-e-apelacao/{documentoReclamacaoApelacao}/permissoes/{tipo}/{id}/destroy', 'ReclamacaoApelacaoPermissoesController@destroy')
        ->name('admin.reclamacaoApelacao.permissoes.destroy');


    Route::get('usuarios', 'UsuariosController@index')
        ->name('admin.usuarios.index');
    Route::post('usuarios/store', 'UsuariosController@store')
        ->name('admin.usuarios.store');
    Route::get('usuarios/{tipo}/{id}/destroy', 'UsuariosController@destroy')
        ->name('admin.usuarios.destroy');

    Route::get('alterar-senha', 'SenhaController@index')
        ->name('admin.alterarSenha');
    Route::post('alterar-senha', 'SenhaController@post')
        ->name('admin.alterarSenha.post');

    Route::post('order', 'AdminController@order')
        ->name('admin.order');
});
