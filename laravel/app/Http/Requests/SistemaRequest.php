<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SistemaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'auditores' => 'required',
            'imagem_auditores' => 'image',
            'inspecoes' => 'required',
            'imagem_inspecoes' => 'image',
            'administrador' => 'required',
            'imagem_administrador' => 'image',
        ];
    }
}
