<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AssuntosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
