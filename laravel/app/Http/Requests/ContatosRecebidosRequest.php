<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assuntos = \App\Models\Assunto::lists('titulo')->toArray();

        return [
            'nome'     => 'required',
            'email'    => 'required|email',
            'assunto'  => 'required|in:'.join(',', $assuntos),
            'mensagem' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required'          => 'Preencha seu :attribute.',
            'assunto.required'  => 'Selecione um assunto.',
            'mensagem.required' => 'Insira uma mensagem.',
            'email.email'       => 'Insira um endereço de e-mail válido.',
            'assunto.in'        => 'Assunto inválido.'
        ];
    }
}
