<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
        ];
    }
}
