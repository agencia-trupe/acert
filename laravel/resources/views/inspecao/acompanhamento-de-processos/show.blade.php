@extends('inspecao.common.template')

@section('content')

    <main class="acompanhamento-de-processos">
        <div class="center">
            <h2 class="title">
                ACOMPANHAMENTO DE PROCESSOS
                <span>
                    {{ $inspecao->cliente }} |
                    {{ $inspecao->obra }} |
                    {{ $inspecao->os }}
                </span>
            </h2>

            @if(!count($inspecao->documentos))
            <div class="info">Nenhum documento encontrado.</div>
            @else
            <div class="table-wrapper">
                <table class="table">
                    <tbody>
                        @foreach($inspecao->documentos as $documento)
                        <tr>
                            <td style="width:20%">
                                {{ $documento->auditor->nome }}
                            </td>
                            <td class="no-padding" style="width:30%">
                                <div class="arquivo-wrapper">
                                    <a href="{{ asset('assets/sistema/inspecao/'.$documento->arquivo) }}" class="icone-arquivo" target="_blank">icone download</a>
                                    {{ $documento->titulo }}
                                </div>
                            </td>
                            <td style="width:50%">{{ $documento->observacoes }}</td>
                            <td>
                                {{ $documento->created_at->format('d/m/Y') }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif

            <p>
                status da auditoria:
                {{ $inspecao->finalizado_em ? 'FINALIZADO' : 'EM PROCESSO' }}
            </p>

            <a href="{{ route('inspecao.acompanhamentoDeProcessos') }}" class="btn btn-default">VOLTAR</a>
        </div>
    </main>

@endsection
