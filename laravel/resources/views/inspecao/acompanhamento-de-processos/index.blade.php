@extends('inspecao.common.template')

@section('content')

    <main class="acompanhamento-de-processos">
        <div class="center">
            <h2 class="title">ACOMPANHAMENTO DE PROCESSOS</h2>

            @if(!count($inspecoes))
            <div class="info info-md">Nenhum processo encontrado.</div>
            @else
            <div class="table-wrapper">
                <table class="table table-md">
                    <tbody>
                        @foreach($inspecoes as $inspecao)
                        <tr data-href="{{ route('inspecao.acompanhamentoDeProcessos.show', $inspecao->id) }}">
                            <td class="stretch no-padding">
                                <div class="arquivo-wrapper">
                                    <div class="icone-inspecao">icone inspecao</div>
                                    <span>
                                        {{ $inspecao->cliente }} |
                                        {{ $inspecao->obra }} |
                                        {{ $inspecao->os }}
                                    </span>
                                </div>
                            </td>
                            <td>
                                {{ $inspecao->created_at->format('d/m/Y') }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </main>

@endsection
