@extends('frontend.common.template')

@section('content')

    <div class="institucional">
        <div class="center">
            @if($empresa->imagem || count($politicas))
            <aside>
                <img src="{{ asset('assets/img/empresa/'.$empresa->imagem) }}" alt="">

                @if(count($politicas))
                <div class="politicas">
                    <h2>Confira nossas Políticas</h2>

                    @foreach($politicas as $p)
                    <a href="{{ route('empresa', $p->slug) }}" @if($politica && $politica->slug === $p->slug) class="active" @endif>
                        {{ $p->titulo }}
                    </a>
                    @endforeach
                </div>
                @endif
            </aside>
            @endif

            <main class="texto">
                @if($politica)
                    <h3>{{ $politica->titulo }}</h3>
                    {!! $politica->texto !!}
                @else
                    {!! $empresa->texto !!}
                @endif
            </main>
        </div>
    </div>

@endsection
