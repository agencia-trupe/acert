@extends('frontend.common.template')

@section('content')

    <div class="institucional">
        <div class="center">
            @if($pagina->imagem)
            <aside>
                <img src="{{ asset('assets/img/paginas/'.$pagina->imagem) }}" alt="">
            </aside>
            @endif

            <main class="texto">
                <h3>{{ $pagina->titulo }}</h3>
                {!! $pagina->texto !!}
            </main>
        </div>
    </div>

@endsection
