@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="center">
            <img src="{{ asset('assets/img/layout/marca-PA-inspecoes.svg') }}" alt="">

            <div class="grid">
                @foreach(range(1, 3) as $i)
                <img src="{{ asset('assets/img/home/'.$home->{"imagem_$i"}) }}" alt="">
                @endforeach
            </div>
        </div>
    </div>

@endsection
