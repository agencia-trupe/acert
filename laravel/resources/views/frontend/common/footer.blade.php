    <footer>
        <div class="center">
            <div class="links">
                <a href="{{ route('home') }}">- HOME</a>
                <a href="{{ route('empresa') }}">- EMPRESA</a>
                <a href="{{ route('contato') }}">- CONTATO</a>
            </div>
            <div class="links">
                @foreach($paginas as $pagina)
                <a href="{{ route('pagina', $pagina->slug) }}">- {{ $pagina->titulo }}</a>
                @endforeach
            </div>

            <div class="acert">
                <a href="{{ route('sistema') }}" class="link-sistema">
                    SISTEMA P A
                </a>

                <img src="{{ asset('assets/img/layout/marca-PA-inspecoes.svg') }}" alt="{{ config('app.name') }}">

                <div class="social">
                    @foreach(['linkedin', 'instagram', 'facebook', 'twitter'] as $s)
                        @if($contato->{$s})
                            <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">
                                {{ $s }}
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>

                <div class="endereco">
                    {{ config('app.name') }}<br>
                    {!! $contato->endereco !!}
                </div>

                <div class="copyright">
                    <p>
                        &copy; {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados
                    </p>
                    <p>
                        <a href="https://www.trupe.net" target="_blank">Criação de Sites</a>:
                        <a href="https://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
