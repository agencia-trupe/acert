    <header class="header-desktop">
        <div class="top-bar">
            <div class="center">
                <nav>
                    <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>HOME</a>
                    <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa')) class="active" @endif>A EMPRESA</a>
                    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>CONTATO</a>
                </nav>

                <div class="social">
                    @foreach(['linkedin', 'instagram', 'facebook', 'twitter'] as $s)
                        @if($contato->{$s})
                            <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">
                                {{ $s }}
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="main">
            <div class="center">
                <?php $middlePageIndex = count($paginas) / 2 + 1; ?>

                <div class="col">
                    @foreach($paginas->slice(0, $middlePageIndex) as $pagina)
                    <a href="{{ route('pagina', $pagina->slug) }}" @if(Tools::routeIs('pagina') && Route::current()->pagina == $pagina->slug) class="active" @endif>
                        {{ $pagina->titulo }}
                    </a>
                    @endforeach
                </div>

                <div class="logo">
                    <img src="{{ asset('assets/img/layout/marca-PA-selo-preto.svg') }}" alt="">
                </div>

                <div class="col">
                    @foreach($paginas->slice($middlePageIndex) as $pagina)
                    <a href="{{ route('pagina', $pagina->slug) }}" @if(Tools::routeIs('pagina') && Route::current()->pagina == $pagina->slug) class="active" @endif>
                        {{ $pagina->titulo }}
                    </a>
                    @endforeach

                    <a href="{{ route('sistema') }}" @if(Tools::routeIs('sistema')) class="active" @endif>
                        SISTEMA P A
                    </a>
                </div>
            </div>
        </div>
    </header>

    <header class="header-mobile">
        <div class="center">
            <a href="{{ route('home') }}" class="logo">
                <img src="{{ asset('assets/img/layout/marca-PA-selo-preto.svg') }}" alt="">
            </a>

            <div>
                <div class="social">
                    @foreach(['linkedin', 'instagram', 'facebook', 'twitter'] as $s)
                        @if($contato->{$s})
                            <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">
                                {{ $s }}
                            </a>
                        @endif
                    @endforeach
                </div>

                <button id="mobile-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>
            </div>
        </div>
    </header>

    <nav id="nav-mobile">
        <div class="center">
            <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>HOME</a>
            <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa')) class="active" @endif>A EMPRESA</a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>CONTATO</a>
            @foreach($paginas as $pagina)
                <a href="{{ route('pagina', $pagina->slug) }}" @if(Tools::routeIs('pagina') && Route::current()->pagina == $pagina->slug) class="active" @endif>
                    {{ $pagina->titulo }}
                </a>
            @endforeach
            <a href="{{ route('sistema') }}" @if(Tools::routeIs('sistema')) class="active" @endif>
                SISTEMA P A
            </a>
        </div>
    </nav>
