@extends('frontend.common.template')

@section('content')

    <div class="sistema">
        <div class="center">
            <h2>BEM-VINDO AO <strong>SISTEMA P A INSPEÇÕES</strong>. SELECIONE O SEU PERFIL DE USUÁRIO:</h2>

            <div class="grid">
                @foreach([
                    'Auditores'     => 'auditoria',
                    'Inspeções'     => 'inspecao',
                    'Administrador' => 'admin',
                ] as $title => $route)
                    <a href="{{ route($route) }}" style="background-image: url({{ asset('assets/img/sistema/'.$sistema->{'imagem_'.str_slug($title)}) }})">
                        <div>
                            <h3>{{ $title }}</h3>
                            <p>{!! $sistema->{str_slug($title)} !!}</p>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
