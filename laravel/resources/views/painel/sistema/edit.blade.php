@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Sistema</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.sistema.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.sistema.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
