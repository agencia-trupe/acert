@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('auditores', 'Auditores') !!}
            {!! Form::textarea('auditores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="well form-group">
            {!! Form::label('imagem_auditores', 'Imagem Auditores') !!}
            @if($registro->imagem_auditores)
            <img src="{{ url('assets/img/sistema/'.$registro->imagem_auditores) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_auditores', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('inspecoes', 'Inspeções') !!}
            {!! Form::textarea('inspecoes', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="well form-group">
            {!! Form::label('imagem_inspecoes', 'Imagem Inspeções') !!}
            @if($registro->imagem_inspecoes)
            <img src="{{ url('assets/img/sistema/'.$registro->imagem_inspecoes) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_inspecoes', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('administrador', 'Administrador') !!}
            {!! Form::textarea('administrador', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="well form-group">
            {!! Form::label('imagem_administrador', 'Imagem Administrador') !!}
            @if($registro->imagem_administrador)
            <img src="{{ url('assets/img/sistema/'.$registro->imagem_administrador) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_administrador', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
