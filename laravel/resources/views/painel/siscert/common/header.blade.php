<div class="c_header">
    <div class="wraps">
        <a href="{{route('painel.siscert.index')}}">
            <div class="image"><img src="{{ asset('assets/img/layout/marca-painspecoes.svg') }}" alt=""></div>
        </a>
        
        <div class="search_tab">
            <h1 class="name">SISTEMA DE <br>ADMINISTRAÇÃO DE CERTIFICAÇÕES</h1>
            <div class="campo">
           
                <form action="{{route('painel.siscert.busca')}}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <input type="text" placeholder="BUSCAR" name="busca" class="search">

                </form>
                <img src="{{ asset('assets/img/layout/icone-buscar.svg') }}" alt="">
            </div>
        </div>
    </div>
</div>