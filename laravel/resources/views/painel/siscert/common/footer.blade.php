<footer>
    <div class="wraps3">
        <div class="f_logo"><img src="{{ asset('assets/img/layout/marca-painspecoes-rodape.svg') }}" alt=""></div>
        <div class="direitos">
            <p>
                &copy; {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados
            </p>
            <p>
                <a href="https://www.trupe.net" target="_blank">Criação de Sites</a>:
                <a href="https://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </div>
</footer>