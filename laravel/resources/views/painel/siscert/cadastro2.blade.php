@extends('painel.siscert.common.template')

@section('content')

<div class="wraps_body">
    <div class="buttons">
        <div class="button3">INSERIR NOVA CERTIFICAÇÃO</div>
        <a href="{{route('painel.siscert.clientes')}}"><div class="button1">GERENCIAR CLIENTES</div></a>
    </div>

    <div class="cadastro">
        <div class="tag">
            <div class="passo1">
                <p>PASSO 1</p>
            </div>
            <div class="titulo">
                <p>CADASTRO DE DADOS PARA O CERTIFICADO</p>
            </div>
        </div>

        <form action="{{route('painel.siscert.update', ['id' => $certifics->id])}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input name='_method' type='hidden' value='put'>

            <div class="ficha">
                <div class="linha1">
                    <p>[SLUG] https://painspecoes.com.br/certif/</p>
                    <input type="text" name="slug" value="{{$certifics->slug}}" disabled>
                </div>

                <div class="linha1">
                    <p>Cliente:</p>
                    <select name="cliente" id="clientes">
                        <option value="{{$certifics->cliente}}" selected>{{$certifics->cliente}}</option>
                        <option value=""disabled>Selecionar novo Cliente:</option>
                        @foreach($clientes as $cliente)
                        <option value="{{$cliente->nome}}">{{$cliente->nome}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="linha1">
                    <p>Título do Projeto:</p>
                    <input type="text" name="projeto" value="{{$certifics->projeto}}">
                </div>

                <div class="linha2">
                    <p>Descritivo do projeto:</p>
                    <textarea id="descritivo" name="descritivo">{{$certifics->descritivo}}</textarea>
                </div>

                <div class="bt_pai">
                    <button type="submit" class="bt_salvar2">SALVAR</button>
                </div>
            </div>


            <div class="parte2">
                <div class="tag">
                    <div class="passo1">
                        <p>PASSO 2</p>
                    </div>

                    <div class="titulo">
                        <p>GERAR QR CODE</p>
                    </div>
                </div>

                <div class="linha1">
                    <p>[SLUG] </p>
                    <input type="text" value="https://painspecoes.com.br/certif/{{$certifics->slug}}">
                </div>

                <a href="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=https%3A%2F%2Fpainspecoes.com.br%2Fcertif%2F{{$certifics->slug}}&choe=UTF-8" style="max-width:200px;display:flex" target="_blank"><div class="bt_salvar">
                    {{-- <a href="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=https%3A%2F%2Fwww.painspecoes.com.br%2Fassets%2Farquivos%2F{{$certifics->slug}}&choe=UTF-8" style="max-width:200px;display:flex" target="_blank"><div class="bt_salvar"> --}}
                    DOWNLOAD DO QR CODE
                </div></a>

                
            </div>

            <div class="parte2">
                <div class="tag">
                    <div class="passo1">
                        <p>PASSO 3</p>
                    </div>

                    <div class="titulo">
                        <p>UPLOAD DE ARQUIVO</p>
                    </div>
                </div>

                <div class="uploads">
                    @if($certifics->arquivo)
                    <a href="{{ url('/assets/arquivos/'.$certifics->arquivo) }}" target="_blank" target="_blank">
                        <p>{{$certifics->arquivo}}</p>
                    </a>
                    @endif
                    <label for="pdf" class="arquivo">SELECIONAR ARQUIVO</label>
                    <input type="file" id="pdf" name="arquivo" style="display: none">
                </div>

                <div class="bt_pai">

                    <button type="submit" class="bt_salvar">SALVAR E FECHAR</button>
                    </form>

                    <form action="{{ route('painel.siscert.cdelete', ['id' => $certifics->id])}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="_method" value="delete" />
                        <button class="bt_excluir" type="submit">EXCLUIR</button>
                    </form>
                </div>

            </div>
        
        
        
    </div>
</div>

<div></div>

@endsection