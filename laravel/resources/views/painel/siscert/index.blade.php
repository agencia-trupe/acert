@extends('painel.siscert.common.template')

@section('content')

<div class="wraps_body">
    <div class="buttons">
        <a href="{{route('painel.siscert.cadastro')}}"><div class="button1">INSERIR NOVA CERTIFICAÇÃO</div></a>
        <a href="{{route('painel.siscert.clientes')}}"><div class="button1">GERENCIAR CLIENTES</div></a>
    </div>

    <div class="archives">
        @foreach($certificacoes as $cert)
        <a href="{{route('painel.siscert.cadastro2', ['id' => $cert->id ])}}">
            <div class="line1">
                <div class="card1">
                    <h3>{{$cert->projeto}}</h3>
                    <p>{{$cert->descritivo}}</p>
                </div>
                <div class="card2">
                    <h3>{{$cert->cliente}}</h3>
                    <p>slug: {{$cert->slug}}</p>
                </div>
                <div class="card3">
                    <h3>{{$cert->created_at->format('d/m/Y')}}</h3>
                </div>
                <div class="card4">
                    <div class="imagers"><img src="{{ asset('assets/img/layout/icone-arquivo-valido.svg') }}" alt=""></div>
                </div>
            </div>
        </a>
        @endforeach
    </div>

</div>

<div></div>

@endsection