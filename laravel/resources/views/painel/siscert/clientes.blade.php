@extends('painel.siscert.common.template')

@section('content')

<div class="wraps_body">
    <div class="buttons">
        <a href="{{route('painel.siscert.cadastro')}}"><div class="button1">INSERIR NOVA CERTIFICAÇÃO</div></a>
        <div class="button3">GERENCIAR CLIENTES</div>
    </div>

    
    <div class="clientes">
        <form action="{{route('painel.siscert.store')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="campo1">
                <p>Inserir cliente:</p>
                <input type="text" name="nome">
                <button type="submit" class="button">SALVAR</button>
                <input type="hidden" name="has_document" value="0">
            </div>
        </form>

        <div class="lista">
            @foreach($clientes as $cliente)
            <div class="line1">
                <div class="descricao">
                    <p>{{$cliente->nome}}</p>
                </div>
                
                @if ($cliente->has_document == '0')  
                <form action="{{ route('painel.siscert.delete', ['id' => $cliente->id])}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method" value="delete" />
                    <div class="excluir">
                        <button class="bgvermelho" type="submit">
                            <img src="{{ asset('assets/img/layout/icone-x.png') }}" alt="">
                        </button>
                    </div>
                </form>
                @endif
            </div>
            @endforeach
        </div>

    </div>
</div>

<div></div>

@endsection