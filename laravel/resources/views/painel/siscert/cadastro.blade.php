@extends('painel.siscert.common.template')

@section('content')

<div class="wraps_body">
    <div class="buttons">
        <div class="button3">INSERIR NOVA CERTIFICAÇÃO</div>
        <a href="{{route('painel.siscert.clientes')}}"><div class="button1">GERENCIAR CLIENTES</div></a>
    </div>

    <div class="cadastro">
        <div class="tag">
            <div class="passo1">
                <p>PASSO 1</p>
            </div>
            <div class="titulo">
                <p>CADASTRO DE DADOS PARA O CERTIFICADO</p>
            </div>
        </div>

        <div class="ficha">
            <form action="{{route('painel.siscert.cstore')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="linha1">
                    <p>[SLUG] https://painspecoes.com.br/certif/</p>
                    <input type="text" name="slug">
                </div>
                <div class="linha1">
                    <p>Cliente:</p>
                    <select name="cliente" id="clientes">
                        <option value="selecionar" selected disabled></option>
                        @foreach($clientes as $cliente)
                        <option value="{{$cliente->nome}}">{{$cliente->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="linha1">
                    <p>Título do Projeto:</p>
                    <input type="text" name="projeto">
                </div>
                <div class="linha2">
                    <p>Descritivo do projeto:</p>
                    <textarea id="descritivo" name="descritivo"></textarea>
                </div>
                <button type="submit" class="bt_salvar">SALVAR</button>
            </form>
        </div>
    </div>
</div>

<div></div>

@endsection