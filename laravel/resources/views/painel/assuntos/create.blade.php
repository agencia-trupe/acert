@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Assuntos /</small> Adicionar Assunto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.assuntos.store', 'files' => true]) !!}

        @include('painel.assuntos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
