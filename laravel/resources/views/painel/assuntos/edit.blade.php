@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Assuntos /</small> Editar Assunto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.assuntos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.assuntos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
