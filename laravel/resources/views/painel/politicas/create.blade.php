@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Políticas /</small> Adicionar Política</h2>
    </legend>

    {!! Form::open(['route' => 'painel.politicas.store', 'files' => true]) !!}

        @include('painel.politicas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
