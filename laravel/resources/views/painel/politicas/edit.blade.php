@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Políticas /</small> Editar Política</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.politicas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.politicas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
