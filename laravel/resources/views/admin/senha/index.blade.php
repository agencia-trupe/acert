@extends('admin.common.template')

@section('content')

    <main>
        <div class="center">
            <form action="{{ route('admin.alterarSenha.post') }}" class="auth" method="POST">
                {!! csrf_field() !!}

                <h2 class="title">ALTERAR SENHA</h2>

                @if($errors->any())
                <div class="flash flash-error">
                    {!! $errors->first() !!}
                </div>
                @endif
                @if(session('success'))
                <div class="flash flash-success">
                    {{ session('success') }}
                </div>
                @endif

                <label>
                    <span>senha atual</span>
                    <input type="password" name="password" required>
                </label>
                <label>
                    <span>nova senha</span>
                    <input type="password" name="nova_senha" required>
                </label>
                <label>
                    <span>confirmar nova senha</span>
                    <input type="password" name="nova_senha_confirmation" required>
                </label>

                <input type="submit" value="SALVAR" class="btn btn-primary">
            </form>
        </div>
    </main>

@endsection
