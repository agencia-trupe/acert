@extends('admin.common.template')

@section('content')

    <main class="documentos-gerais">
        <div class="center">
            <h2 class="title">DOCUMENTOS GERAIS</h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.documentosGerais.store') }}" method="POST" enctype="multipart/form-data" class="form-documento-admin">
                {!! csrf_field() !!}

                <div class="row">
                    <span><strong>ADICIONANDO DOCUMENTO:</strong></span>
                    <label class="file-input">
                        <span>SELECIONAR DOCUMENTO</span>
                        <input type="file" name="arquivo" required>
                    </label>
                </div>
                <div class="row">
                    <span>descrição/título:</span>
                    <input type="text" name="titulo" value="{{ old('titulo') }}" required>
                    <input type="submit" value="ENVIAR" class="btn btn-primary">
                </div>
            </form>

            @if(!count($documentos))
            <div class="info">Nenhum documento encontrado.</div>
            @else
            <div class="table-wrapper">
                <table class="table table-sortable" data-table="sistema_documentos_gerais" data-order-url="{{ route('admin.order') }}">
                    <tbody>
                        @foreach($documentos as $documento)
                        <tr class="tr-row" id="{{ $documento->id }}">
                            <td class="handle-ordem">
                                <a href="#" class="btn-ordem">icone ordem</a>
                            </td>
                            <td class="stretch no-padding">
                                <div class="arquivo-wrapper">
                                    <a href="{{ asset('assets/sistema/documentos-gerais/'.$documento->arquivo) }}" class="icone-arquivo" target="_blank">icone download</a>
                                    {{ $documento->titulo }}
                                </div>
                            </td>
                            <td>
                                {{ $documento->created_at->format('d/m/Y') }}
                            </td>
                            <td class="controls">
                                <a href="{{ route('admin.documentosGerais.permissoes', $documento->id) }}" class="icone-permissoes">permissões</a>
                                <a href="{{ route('admin.documentosGerais.edit', $documento->id) }}" class="icone-editar">editar</a>
                                <a href="{{ route('admin.documentosGerais.destroy', $documento->id) }}" class="icone-excluir link-excluir">excluir</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </main>

@endsection
