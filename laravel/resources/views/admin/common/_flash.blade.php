@if(session('success'))
    <div class="flash flash-success">
        {!! session('success') !!}
    </div>
@endif

@if($errors->any())
    <div class="flash flash-error">
        @foreach($errors->all() as $error)
        {!! $error !!}<br>
        @endforeach
    </div>
@endif

