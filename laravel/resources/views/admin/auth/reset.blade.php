@extends('admin.common.template')

@section('content')

    <main>
        <div class="center">
            <form action="{{ route('admin.password.reset') }}" class="auth" method="POST">
                {!! csrf_field() !!}

                <h2 class="title">REDEFINIR SENHA</h2>

                @if($errors->any())
                <div class="flash flash-error">
                    {!! $errors->first() !!}
                </div>
                @endif

                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}" required>

                <label>
                    <span>senha</span>
                    <input type="password" name="password" required>
                </label>
                <label>
                    <span>confirmar senha</span>
                    <input type="password" name="password_confirmation" required>
                </label>

                <input type="submit" value="REDEFINIR SENHA" class="btn btn-primary">
            </form>
        </div>
    </main>

@endsection
