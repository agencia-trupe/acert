@extends('admin.common.template')

@section('content')

    <main class="login">
        <div class="center">
            <form action="{{ route('admin.login') }}" class="auth" method="POST">
                {!! csrf_field() !!}

                <h2 class="title">ADMINISTRADOR P A</h2>

                @if($errors->any())
                <div class="flash flash-error">
                    E-mail ou senha inválidos.
                </div>
                @endif
                @if (session('status'))
                <div class="flash flash-success">
                    {{ session('status') }}
                </div>
                @endif

                <label>
                    <span>e-mail</span>
                    <input type="email" name="email" value="{{ old('email') }}" required>
                </label>
                <label>
                    <span>senha</span>
                    <input type="password" name="password" required>
                </label>

                <input type="submit" value="ENTRAR" class="btn btn-primary">

                <a href="{{ route('admin.password.linkRequest') }}">
                    esqueci minha senha >
                </a>
            </form>
        </div>
    </main>

@endsection
