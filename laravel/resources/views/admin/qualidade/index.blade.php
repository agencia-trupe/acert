@extends('admin.common.template')

@section('content')

    <main class="qualidade">
        <div class="center">
            <h2 class="title">QUALIDADE</h2>

            @include('admin.common._flash')

            @if(!count($categorias))
            <div class="info info-md">Nenhuma categoria encontrada.</div>
            @else
            <div class="table-wrapper">
                <table class="table table-md table-sortable" data-table="sistema_qualidade_categorias" data-order-url="{{ route('admin.order') }}">
                    <tbody>
                        @foreach($categorias as $categoria)
                        <tr class="tr-row" id="{{ $categoria->id }}">
                            <td class="handle-ordem">
                                <a href="#" class="btn-ordem">icone ordem</a>
                            </td>
                            <td class="stretch categoria" data-href="{{ route('admin.qualidade.documentos.index', $categoria->id) }}">
                                {{ $categoria->titulo }}
                            </td>
                            <td class="controls">
                                <a href="{{ route('admin.qualidade.edit', $categoria->id) }}" class="icone-editar">editar</a>
                                <a href="{{ route('admin.qualidade.destroy', $categoria->id) }}" class="icone-excluir link-excluir">excluir</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif

            <form action="{{ route('admin.qualidade.store') }}" method="POST" enctype="multipart/form-data" class="form-categoria">
                {!! csrf_field() !!}

                <input type="text" name="titulo" placeholder="título" value="{{ old('titulo') }}" required>
                <input type="submit" value="ADICIONAR CATEGORIA" class="btn btn-default">
            </form>
        </div>
    </main>

@endsection
