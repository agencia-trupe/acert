@extends('admin.common.template')

@section('content')

    <main class="inspecao">
        <div class="center">
            <h2 class="title">
                QUALIDADE ||
                {{ $qualidadeCategoria->titulo }} |
                {{ $qualidadeDocumento->titulo }}
                <span>PERMISSÕES</span>
            </h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.qualidade.documentos.permissoes.store', [$qualidadeCategoria->id, $qualidadeDocumento->id]) }}" method="POST" class="form-documento-admin">
                {!! csrf_field() !!}

                <h3>CADASTRAR PERMISSÃO</h3>

                <input type="hidden" name="perfil" value="auditor-de-campo">
                <div class="row">
                    <span>Usuário (Auditor de Campo):</span>
                    <select name="usuario" required>
                        <option value="">Selecione...</option>
                        @foreach($usuarios as $usuario)
                        <option value="{{ $usuario->id }}">{{ $usuario->nome }} ({{ $usuario->email }})</option>
                        @endforeach
                    </select>
                    <input type="submit" value="CADASTRAR" class="btn btn-primary">
                </div>
            </form>

            @if(!count($qualidadeDocumento->permissoes))
            <div class="info">Nenhuma permissão cadastrada.</div>
            @else
            <div class="table-wrapper">
                <table class="table">
                    @foreach($qualidadeDocumento->permissoes as $usuario)
                    <tr>
                        <td class="stretch">{{ $usuario->nome }}</td>
                        <td class="stretch">{{ $usuario->email }}</td>
                        <td>
                            <a href="{{ route('admin.qualidade.documentos.permissoes.destroy', [$qualidadeCategoria->id, $qualidadeDocumento->id, str_slug($usuario->tipo), $usuario->id]) }}" class="icone-excluir link-excluir">excluir</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            @endif

            <a href="{{ route('admin.qualidade.documentos.index', $qualidadeCategoria->id) }}" class="btn btn-default btn-voltar">VOLTAR</a>
        </div>
    </main>

@endsection
