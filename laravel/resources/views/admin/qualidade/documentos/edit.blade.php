@extends('admin.common.template')

@section('content')

    <main class="qualidade">
        <div class="center">
            <h2 class="title">QUALIDADE | {{ $qualidadeCategoria->titulo }}</h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.qualidade.documentos.update', [$qualidadeCategoria->id, $qualidadeDocumento->id]) }}" method="POST" enctype="multipart/form-data" class="form-documento-admin">
                {!! csrf_field() !!}
                {!! method_field('PATCH') !!}

                <div class="row">
                    <span><strong>ALTERAR DOCUMENTO:</strong></span>
                    <label class="file-input">
                        <span>SELECIONAR DOCUMENTO</span>
                        <input type="file" name="arquivo">
                    </label>
                </div>
                <div class="row link-documento-editar">
                    <span></span>
                    <a href="{{ asset('assets/sistema/qualidade/'.$qualidadeDocumento->arquivo) }}" target="_blank">{{ $qualidadeDocumento->arquivo }}</a>
                </div>
                <div class="row">
                    <span>descrição/título:</span>
                    <input type="text" name="titulo" value="{{ $qualidadeDocumento->titulo }}" required>
                    <input type="submit" value="SALVAR" class="btn btn-primary">
                </div>

                <a href="{{ route('admin.qualidade.documentos.index', $qualidadeCategoria->id) }}" class="btn btn-default btn-voltar">VOLTAR</a>
            </form>
        </div>
    </main>

@endsection
