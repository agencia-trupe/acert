@extends('admin.common.template')

@section('content')

    <main class="qualidade">
        <div class="center">
            <h2 class="title">QUALIDADE</h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.qualidade.update', $qualidadeCategoria->id) }}" method="POST" enctype="multipart/form-data" class="form-categoria">
                {!! csrf_field() !!}
                {!! method_field('PATCH') !!}

                <input type="text" name="titulo" placeholder="título" value="{{ $qualidadeCategoria->titulo }}" required>
                <input type="submit" value="SALVAR CATEGORIA" class="btn btn-default">

                <a href="{{ route('admin.qualidade.index') }}" class="btn btn-default btn-voltar">VOLTAR</a>
            </form>
        </div>
    </main>

@endsection
