@extends('admin.common.template')

@section('content')

    <main class="inspecao">
        <div class="center">
            <h2 class="title">
                INSPEÇÃO ||
                {{ $inspecao->cliente }} |
                {{ $inspecao->obra }} |
                {{ $inspecao->os }}
                <span>PERMISSÕES</span>
            </h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.inspecao.permissoes.store', $inspecao->id) }}" method="POST" class="form-documento-admin form-inspecao-permissoes">
                {!! csrf_field() !!}

                <h3>CADASTRAR PERMISSÃO</h3>

                <div class="row">
                    <span>Perfil:</span>
                    <select name="perfil" required>
                        <option value="">Selecione...</option>
                        <option value="auditor-de-campo">Auditor de Campo</option>
                        <option value="acesso-inspecao">Acesso Inspeção</option>
                    </select>
                </div>
                <div class="row">
                    <span>Usuário:</span>
                    <select name="usuario" required>
                        <option value="">Selecione...</option>
                        @foreach($usuarios as $perfil => $usuarios)
                            @foreach($usuarios as $usuario)
                            <option value="{{ $usuario->id }}" data-perfil={{ $perfil }}>{{ $usuario->nome }} ({{ $usuario->email }})</option>
                            @endforeach
                        @endforeach
                    </select>
                    <input type="submit" value="CADASTRAR" class="btn btn-primary">
                </div>
            </form>

            @if(!count($inspecao->permissoes))
            <div class="info">Nenhuma permissão cadastrada.</div>
            @else
            <div class="table-wrapper">
                <table class="table">
                    @foreach($inspecao->permissoes as $usuario)
                    <tr>
                        <td class="stretch">{{ $usuario->nome }}</td>
                        <td class="stretch">{{ $usuario->email }}</td>
                        <td style="white-space:nowrap">
                            {{ mb_strtoupper($usuario->tipo) }}
                        </td>
                        <td>
                            <a href="{{ route('admin.inspecao.permissoes.destroy', [$inspecao->id,str_slug($usuario->tipo), $usuario->id]) }}" class="icone-excluir link-excluir">excluir</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            @endif

            <a href="{{ route('admin.inspecao.index') }}" class="btn btn-default btn-voltar">VOLTAR</a>
        </div>
    </main>

@endsection
