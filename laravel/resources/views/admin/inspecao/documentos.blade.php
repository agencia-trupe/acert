@extends('admin.common.template')

@section('content')

    <main class="inspecoes">
        <div class="center">
            <h2 class="title">
                INSPEÇÃO ||
                {{ $inspecao->cliente }} |
                {{ $inspecao->obra }} |
                {{ $inspecao->os }}
                <span>
                    {{ $inspecao->finalizado_em ? 'FINALIZADO' : 'EM PROCESSO' }}
                </span>
            </h2>

            @include('admin.common._flash')

            @if(!count($inspecao->documentos))
            <div class="info">Nenhum documento encontrado.</div>
            @else
            <div class="table-wrapper">
                <table class="table">
                    <tbody>
                        @foreach($inspecao->documentos as $documento)
                        <tr>
                            <td style="width:20%">
                                {{ $documento->auditor->nome }}
                            </td>
                            <td class="no-padding" style="width:30%">
                                <div class="arquivo-wrapper">
                                    <a href="{{ asset('assets/sistema/inspecao/'.$documento->arquivo) }}" class="icone-arquivo" target="_blank">icone download</a>
                                    {{ $documento->titulo }}
                                </div>
                            </td>
                            <td style="width:50%">{{ $documento->observacoes }}</td>
                            <td>
                                {{ $documento->created_at->format('d/m/Y') }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif

            <div class="btn-inline-group btn-voltar">
                @if($inspecao->finalizado_em)
                    <a href="{{ route('admin.inspecao.reabrir', $inspecao->id) }}" class="btn btn-primary link-reabrir">REABRIR INSPEÇÃO</a>
                @elseif(! $inspecao->finalizado_em && count($inspecao->documentos))
                    <a href="{{ route('admin.inspecao.finalizar', $inspecao->id) }}" class="btn btn-primary link-finalizar">FINALIZAR INSPEÇÃO</a>
                @endif
                <a href="{{ route('admin.inspecao.index') }}" class="btn btn-default">VOLTAR</a>
            </div>
        </div>
    </main>

@endsection
