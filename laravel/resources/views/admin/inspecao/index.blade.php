@extends('admin.common.template')

@section('content')

    <main class="inspecao">
        <div class="center">
            <h2 class="title">INSPEÇÃO</h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.inspecao.store') }}" method="POST" enctype="multipart/form-data" class="form-documento-admin form-documento-inspecao">
                {!! csrf_field() !!}

                <h3>CADASTRAR NOVA INSPEÇÃO</h3>
                <div class="row">
                    <span>Nome do Cliente:</span>
                    <input type="text" name="cliente" value="{{ old('cliente') }}" required>
                </div>
                <div class="row">
                    <span>Nome da Obra:</span>
                    <input type="text" name="obra" value="{{ old('obra') }}" required>
                </div>
                <div class="row">
                    <span>Número da OS:</span>
                    <input type="text" name="os" value="{{ old('os') }}" required>
                    <input type="submit" value="CADASTRAR" class="btn btn-primary">
                </div>
            </form>

            <form action="{{ route('admin.inspecao.index') }}" method="GET">
                <div class="table-wrapper">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="td-busca">
                                    <div class="busca">
                                        <input type="text" name="cliente" value="{{ request('cliente') }}" placeholder="buscar">
                                        <input type="submit">
                                    </div>
                                </td>
                                <td class="td-busca">
                                    <div class="busca">
                                        <input type="text" name="obra" value="{{ request('obra') }}" placeholder="buscar">
                                        <input type="submit">
                                    </div>
                                </td>
                                <td class="td-busca">
                                    <div class="busca">
                                        <input type="text" name="os" value="{{ request('os') }}" placeholder="buscar">
                                        <input type="submit">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                @foreach(['cliente', 'obra', 'os', 'finalizado_em', 'created_at'] as $coluna)
                                <td class="td-ordenar">
                                    <a href="{{ route('admin.inspecao.index', array_merge($_GET, [
                                        'ordem' => $coluna,
                                        'dir'   => request('ordem') == $coluna && request('dir') == 'asc' ? 'desc' : 'asc'
                                    ])) }}" class="{{ request('ordem') == $coluna && request('dir') == 'asc' ? 'desc' : 'asc' }} {{ request('ordem') == $coluna ? 'active' : '' }}">
                                        ordenar
                                    </a>
                                </td>
                                @endforeach
                            </tr>
                            @foreach($inspecoes as $inspecao)
                            <tr data-href="{{ route('admin.inspecao.documentos', $inspecao->id) }}">
                                <td style="width:40%">{{ $inspecao->cliente }}</td>
                                <td style="width:40%">{{ $inspecao->obra }}</td>
                                <td style="width:20%">{{ $inspecao->os }}</td>
                                <td style="white-space:nowrap">{{ $inspecao->finalizado_em ? 'FINALIZADO' : 'EM PROCESSO' }}</td>
                                <td>
                                    {{ $inspecao->created_at->format('d/m/Y') }}
                                </td>
                                <td class="controls">
                                    <a href="{{ route('admin.inspecao.permissoes', $inspecao->id) }}" class="icone-permissoes">permissões</a>
                                    <a href="{{ route('admin.inspecao.edit', $inspecao->id) }}" class="icone-editar">editar</a>
                                    <a href="{{ route('admin.inspecao.destroy', $inspecao->id) }}" class="icone-excluir link-excluir">excluir</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </form>

            @if(!count($inspecoes))
            <div class="info">Nenhuma inspeção encontrada.</div>
            @endif
        </div>
    </main>

@endsection
