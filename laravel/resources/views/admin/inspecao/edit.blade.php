@extends('admin.common.template')

@section('content')

    <main class="inspecao">
        <div class="center">
            <h2 class="title">INSPEÇÃO</h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.inspecao.update', $inspecao->id) }}" method="POST" enctype="multipart/form-data" class="form-documento-admin form-documento-inspecao">
                {!! csrf_field() !!}
                {!! method_field('PATCH') !!}

                <h3>ALTERAR INSPEÇÃO</h3>
                <div class="row">
                    <span>Nome do Cliente:</span>
                    <input type="text" name="cliente" value="{{ $inspecao->cliente }}" required>
                </div>
                <div class="row">
                    <span>Nome da Obra:</span>
                    <input type="text" name="obra" value="{{ $inspecao->obra }}" required>
                </div>
                <div class="row">
                    <span>Número da OS:</span>
                    <input type="text" name="os" value="{{ $inspecao->os }}" required>
                    <input type="submit" value="SALVAR" class="btn btn-primary">
                </div>

                <a href="{{ route('admin.inspecao.index') }}" class="btn btn-default btn-voltar">VOLTAR</a>
            </form>
        </div>
    </main>

@endsection
