@extends('admin.common.template')

@section('content')

    <main class="inspecao">
        <div class="center">
            <h2 class="title">
                RECLAMAÇÃO E APELAÇÃO ||
                {{ $documentoReclamacaoApelacao->titulo }}
                <span>PERMISSÕES</span>
            </h2>

            @include('admin.common._flash')

            <form action="{{ route('admin.reclamacaoApelacao.permissoes.store', $documentoReclamacaoApelacao->id) }}" method="POST" class="form-documento-admin">
                {!! csrf_field() !!}

                <h3>CADASTRAR PERMISSÃO</h3>

                <input type="hidden" name="perfil" value="acesso-inspecao">
                <div class="row">
                    <span>Usuário (Acesso Inspeção):</span>
                    <select name="usuario" required>
                        <option value="">Selecione...</option>
                        @foreach($usuarios as $usuario)
                        <option value="{{ $usuario->id }}">{{ $usuario->nome }} ({{ $usuario->email }})</option>
                        @endforeach
                    </select>
                    <input type="submit" value="CADASTRAR" class="btn btn-primary">
                </div>
            </form>

            @if(!count($documentoReclamacaoApelacao->permissoes))
            <div class="info">Nenhuma permissão cadastrada.</div>
            @else
            <div class="table-wrapper">
                <table class="table">
                    @foreach($documentoReclamacaoApelacao->permissoes as $usuario)
                    <tr>
                        <td class="stretch">{{ $usuario->nome }}</td>
                        <td class="stretch">{{ $usuario->email }}</td>
                        <td>
                            <a href="{{ route('admin.reclamacaoApelacao.permissoes.destroy', [$documentoReclamacaoApelacao->id,str_slug($usuario->tipo), $usuario->id]) }}" class="icone-excluir link-excluir">excluir</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            @endif

            <a href="{{ route('admin.reclamacaoApelacao.index') }}" class="btn btn-default btn-voltar">VOLTAR</a>
        </div>
    </main>

@endsection
