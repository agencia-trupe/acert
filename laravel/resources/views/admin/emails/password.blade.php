<!DOCTYPE html>
<html>
<head>
    <title>Redefinição de senha</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='color:#000;font-size:14px;font-family:Verdana;'>
        <a href="{{ route('admin.password.resetForm', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">Clique aqui para redefinir sua senha.</a>
    </span>
</body>
</html>
