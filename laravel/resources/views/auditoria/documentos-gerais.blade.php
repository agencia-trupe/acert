@extends('auditoria.common.template')

@section('content')

    <main class="documentos-gerais">
        <div class="center">
            <h2 class="title">DOCUMENTOS GERAIS</h2>

            @if(!count($documentos))
            <div class="info info-md">Nenhum documento encontrado.</div>
            @else
            <table class="table table-md">
                <tbody>
                    @foreach($documentos as $documento)
                    <tr>
                        <td class="stretch no-padding">
                            <div class="arquivo-wrapper">
                                <a href="{{ asset('assets/sistema/documentos-gerais/'.$documento->arquivo) }}" class="icone-arquivo" target="_blank">icone download</a>
                                {{ $documento->titulo }}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </main>

@endsection
