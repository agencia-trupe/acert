@extends('auditoria.common.template')

@section('content')

    <main class="qualidade">
        <div class="center">
            <h2 class="title">QUALIDADE</h2>

            @if(!count($categorias))
            <div class="info info-md">Nenhuma categoria encontrada.</div>
            @else
            @foreach($categorias as $categoria)
            <div class="qualidade-categoria-handle">
                <span>{{ $categoria->titulo }}</span>
                <input type="text" name="buscar" placeholder="buscar">
            </div>
            <table class="table table-md">
                <tbody>
                    @foreach($categoria->documentos as $documento)
                    <tr>
                        <td class="stretch no-padding">
                            <div class="arquivo-wrapper">
                                <a href="{{ asset('assets/sistema/qualidade/'.$documento->arquivo) }}" class="icone-arquivo" target="_blank">icone download</a>
                                <span>{{ $documento->titulo }}</span>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endforeach
            @endif
        </div>
    </main>

@endsection
