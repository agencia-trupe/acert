@extends('auditoria.common.template')

@section('content')

    <main class="inspecao">
        <div class="center">
            <div class="inspecao-show-header">
                <h2 class="title">
                    INSPEÇÃO -
                    {{ $inspecao->cliente }} |
                    {{ $inspecao->obra }} |
                    {{ $inspecao->os }}
                    <span>
                        {{ $inspecao->finalizado_em ? 'FINALIZADO' : 'EM PROCESSO' }}
                    </span>
                </h2>

                @unless($inspecao->finalizado_em)
                <a href="{{ route('auditoria.inspecao.create', $inspecao->id) }}" class="btn btn-primary">ADICIONAR DOCUMENTO</a>
                @endunless
            </div>

            @include('auditoria.common._flash')

            @if(!count($inspecao->documentos))
            <div class="info">Nenhum documento encontrado.</div>
            @else
            <div class="table-wrapper">
                <table class="table table-sortable" data-table="sistema_inspecoes_documentos" data-order-url="{{ route('auditoria.inspecao.order', $inspecao->id) }}">
                    <tbody>
                        @foreach($inspecao->documentos as $documento)
                        <tr class="tr-row" id="{{ $documento->id }}">
                            @unless($inspecao->finalizado_em)
                            <td class="handle-ordem">
                                <a href="#" class="btn-ordem">icone ordem</a>
                            </td>
                            @endunless
                            <td class="no-padding" style="width:40%">
                                <div class="arquivo-wrapper">
                                    <a href="{{ asset('assets/sistema/inspecao/'.$documento->arquivo) }}" class="icone-arquivo" target="_blank">icone download</a>
                                    <span>{{ $documento->titulo }}</span>
                                </div>
                            </td>
                            <td style="width:60%">
                                {{ $documento->observacoes }}
                            </td>
                            <td>
                                {{ $documento->created_at->format('d/m/Y') }}
                            </td>
                            @unless($inspecao->finalizado_em)
                            <td class="controls">
                                <a href="{{ route('auditoria.inspecao.edit', [$inspecao->id, $documento->id]) }}" class="icone-editar">editar</a>
                                <a href="{{ route('auditoria.inspecao.destroy', [$inspecao->id, $documento->id]) }}" class="icone-excluir link-excluir">excluir</a>
                            </td>
                            @endunless
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif

            <div class="btn-inline-group btn-voltar">
                @unless($inspecao->finalizado_em || !count($inspecao->documentos))
                <a href="{{ route('auditoria.inspecao.finalizar', $inspecao->id) }}" class="btn btn-primary link-finalizar">FINALIZAR INSPEÇÃO</a>
                @endunless
                <a href="{{ route('auditoria.inspecao') }}" class="btn btn-default">VOLTAR</a>
            </div>
        </div>
    </main>

@endsection
