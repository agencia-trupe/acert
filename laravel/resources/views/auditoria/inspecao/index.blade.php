@extends('auditoria.common.template')

@section('content')

    <main class="inspecao">
        <div class="center">
            <h2 class="title">INSPEÇÃO</h2>

            @if(!count($emProcesso) && !count($finalizados))
            <div class="info info-md">Nenhuma inspeção encontrada.</div>
            @endif
            @if(count($emProcesso))
                <div class="inspecao-lista-header">EM PROCESSO</div>
                <table class="table table-md">
                    <tbody>
                        @foreach($emProcesso as $inspecao)
                        <tr data-href="{{ route('auditoria.inspecao.show', $inspecao->id) }}">
                            <td class="stretch no-padding">
                                <div class="arquivo-wrapper">
                                    <div class="icone-inspecao">
                                        icone inspeção
                                    </div>
                                    <span>
                                        {{ $inspecao->cliente }} |
                                        {{ $inspecao->obra }} |
                                        {{ $inspecao->os }}
                                    </span>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif

            @if(count($finalizados))
                <div class="inspecao-lista-header">FINALIZADOS</div>
                <table class="table table-md">
                    <tbody>
                        @foreach($finalizados as $inspecao)
                        <tr data-href="{{ route('auditoria.inspecao.show', $inspecao->id) }}">
                            <td class="stretch no-padding">
                                <div class="arquivo-wrapper">
                                    <div class="icone-inspecao">
                                        icone inspeção
                                    </div>
                                    <span>
                                        {{ $inspecao->cliente }} |
                                        {{ $inspecao->obra }} |
                                        {{ $inspecao->os }}
                                    </span>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </main>

@endsection
