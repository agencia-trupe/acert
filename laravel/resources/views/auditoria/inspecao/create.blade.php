@extends('auditoria.common.template')

@section('content')

    <main class="inspecao">
        <div class="center">
            <h2 class="title">
                INSPEÇÃO -
                {{ $inspecao->cliente }} |
                {{ $inspecao->obra }} |
                {{ $inspecao->os }}
            </h2>

            @include('auditoria.common._flash')

            <form action="{{ route('auditoria.inspecao.store', $inspecao->id) }}" method="POST" enctype="multipart/form-data" class="form-documento-admin">
                {!! csrf_field() !!}

                <div class="row">
                    <span><strong>ADICIONANDO DOCUMENTO:</strong></span>
                    <label class="file-input">
                        <span>SELECIONAR DOCUMENTO</span>
                        <input type="file" name="arquivo" required>
                    </label>
                </div>
                <div class="row">
                    <span>descrição/título:</span>
                    <input type="text" name="titulo" value="{{ old('titulo') }}" required>
                </div>
                <div class="row">
                    <span>observações:</span>
                    <input type="text" name="observacoes" value="{{ old('observacoes') }}">
                </div>
                <div class="row">
                    <span></span>
                    <div class="btn-inline-group">
                        <input type="submit" value="SALVAR" class="btn btn-primary">
                        <a href="{{ route('auditoria.inspecao.show', $inspecao->id) }}" class="btn btn-default">VOLTAR</a>
                    </div>
                </div>
            </form>
        </div>
    </main>

@endsection
