@extends('auditoria.common.template')

@section('content')

    <main class="login">
        <div class="center">
            <form action="{{ route('auditoria.login') }}" class="auth" method="POST">
                {!! csrf_field() !!}

                <h2 class="title">AUDITOR</h2>

                @if($errors->any())
                <div class="flash flash-error">
                    E-mail ou senha inválidos.
                </div>
                @endif
                @if (session('status'))
                <div class="flash flash-success">
                    {{ session('status') }}
                </div>
                @endif

                <label>
                    <span>e-mail</span>
                    <input type="email" name="email" value="{{ old('email') }}" required>
                </label>
                <label>
                    <span>senha</span>
                    <input type="password" name="password" required>
                </label>

                <input type="submit" value="ENTRAR" class="btn btn-primary">

                <a href="{{ route('auditoria.password.linkRequest') }}">
                    esqueci minha senha >
                </a>
            </form>
        </div>
    </main>

@endsection
