@extends('auditoria.common.template')

@section('content')

    <main class="login">
        <div class="center">
            <form action="{{ route('auditoria.password.sendResetLink') }}" class="auth" method="POST">
                {!! csrf_field() !!}

                <h2 class="title">ESQUECI MINHA SENHA</h2>

                @if($errors->any())
                <div class="flash flash-error">
                    {{ $errors->first() }}
                </div>
                @endif
                @if (session('status'))
                <div class="flash flash-success">
                    {{ session('status') }}
                </div>
                @endif

                <label>
                    <span>e-mail</span>
                    <input type="email" name="email" value="{{ old('email') }}" required>
                </label>

                <input type="submit" value="REDEFINIR SENHA" class="btn btn-primary">
            </form>
        </div>
    </main>

@endsection
