<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Senha alterada com sucesso!',
    'sent' => 'Um e-mail foi enviado com instruções para a redefinição de senha.',
    'token' => 'Token inválido.',
    'user' => "E-mail não encontrado.",

];
