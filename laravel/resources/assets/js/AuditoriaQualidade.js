export default function AuditoriaQualidade() {
    $('.qualidade-categoria-handle').click(function() {
        if ($(this).hasClass('active')) return;

        $('.qualidade-categoria-handle')
            .removeClass('active')
            .next()
            .hide();
        $(this)
            .addClass('active')
            .next()
            .show();
    });

    $('.qualidade-categoria-handle input').keyup(function() {
        var val = $.trim($(this).val())
            .replace(/ +/g, ' ')
            .toLowerCase();

        $(this)
            .parent()
            .next()
            .find('tr')
            .show()
            .filter(function() {
                var text = $(this)
                    .text()
                    .replace(/\s+/g, ' ')
                    .toLowerCase();
                return !~text.indexOf(val);
            })
            .hide();
    });
}
