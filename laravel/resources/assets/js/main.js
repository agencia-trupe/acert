import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Confirmacao from './Confirmacao';
import Ordenacao from './Ordenacao';
import LinhaClicavel from './LinhaClicavel';
import FileInput from './FileInput';
import AuditoriaQualidade from './AuditoriaQualidade';
import InspecaoPermissoes from './InspecaoPermissoes';

AjaxSetup();
MobileToggle();
Confirmacao();
Ordenacao();
LinhaClicavel();
FileInput();
AuditoriaQualidade();
InspecaoPermissoes();


