export default function FileInput() {
    if ($('.file-input').length) {
        var textoPadrao = $('.file-input span')
            .first()
            .text();

        $('.file-input input').change(function(event) {
            if (event.target.files[0]) {
                $(this)
                    .parent()
                    .find('span')
                    .text(event.target.files[0].name)
                    .addClass('ativo');
            } else {
                $(this)
                    .parent()
                    .find('span')
                    .text(textoPadrao)
                    .removeClass('ativo');
            }
        });
    }
}
