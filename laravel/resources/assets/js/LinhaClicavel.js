export default function LinhaClicavel() {
    $('tr[data-href], td[data-href]').click(function() {
        location.href = $(this).data('href');
    });
}
