import swal from 'sweetalert';

export default function Confirmacao() {
    $('.link-excluir').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        var url = $(this).attr('href');

        console.log(url);

        swal({
            title: '',
            text: 'Deseja excluir o registro?',
            buttons: ['Cancelar', 'Excluir'],
            dangerMode: true
        }).then(willDelete => {
            if (willDelete) location.href = url;
        });
    });

    $('.link-finalizar').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        var url = $(this).attr('href');

        console.log(url);

        swal({
            title: '',
            text: 'Deseja finalizar a inspeção?',
            buttons: {
                cancel: {
                    text: 'Cancelar',
                    visible: true
                },
                confirm: {
                    text: 'Finalizar',
                    className: 'swal-btn-finalizar'
                }
            },
            dangerMode: true
        }).then(confirmed => {
            if (confirmed) location.href = url;
        });
    });

    $('.link-reabrir').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        var url = $(this).attr('href');

        console.log(url);

        swal({
            title: '',
            text: 'Deseja reabrir a inspeção?',
            buttons: {
                cancel: {
                    text: 'Cancelar',
                    visible: true
                },
                confirm: {
                    text: 'Reabrir',
                    className: 'swal-btn-finalizar'
                }
            },
            dangerMode: true
        }).then(confirmed => {
            if (confirmed) location.href = url;
        });
    });
}
